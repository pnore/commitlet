$(document).ready(function() {
    paramsForJs = (paramsForJs === null || paramsForJs === undefined)? 'did not get it' : paramsForJs;
	$(document).ajaxStart(function() {$.blockUI({ 
		message: "<h1 class='loading'>loading...</h1><div class='loading'></div>",
		showOverlay: false,
        centerY: false,
		css: {
			padding:        5, 
			margin:         0, 
			width:          '300px', 
			height:			'100px',
			top:			'',
			right:          '10px', 
			bottom:         '50px', 
			left:           '', 
			textAlign:      'center', 
			color:          '#000', 
			border:         'none', 
			opacity: 		0.7,
			backgroundColor:'#fff', 
			cursor:         'null',
			'-webkit-border-radius': '10px', 
			'-moz-border-radius':    '10px' 
		},
	})});
    $(document).ajaxStop($.unblockUI);
    $('table').click(tableClicks);
	setupTimers();
	updateTimers();
});

function updateTimers() {
	
}

function setupTimers() {
	var biggestDiff = 0;
	var today = new Date().valueOf();
	$('div.time-remaining').each( function(idx, div) {
		//var toDate = parseInt($(div).text())*1000;
		var toDate = $(div).attr('due');
    	toDate = new Date( toDate );
		var thisDiff = toDate.valueOf() - today;
		if( thisDiff > biggestDiff ) { biggestDiff = thisDiff; }
		var month = toDate.getMonth();
		var day = toDate.getDate();
		var until = getTimeUntil(toDate);
		var untilPhrase = getHumanReadableIntervalText(until); 
		$(div).html(untilPhrase);
		if( $(div).prev().hasClass('invalid') && 
			$(div).hasClass('gone') ) {
        	$(div).removeClass('gone');
		}
			});
	biggestDiff = biggestDiff / 1000 / 60 / 60 / 24;
	//alert( 'biggestDiff was ' + biggestDiff + ' days');
}

// TODO: make a dateperiod object that encapsulates the data formally
function getHumanReadableIntervalText( getTimeUntilArray ) {
	var intervals = [ ' d', ' hr', ' min', ' sec' ];
	var a = getTimeUntilArray; 
	var ret = '';
	for( var i = 0; i < intervals.length; i++ ) {
		if( a[i] > 0 ) {
			ret = ret + a[i] + intervals[i] + '<br/>';
			/*
			var ret = a[i] + intervals[i] + (( a[i] == 1 ) ? '' : 's');
			if( i < intervals.length-1 ) {
				i = i+1;
				ret = ret + ', ' +  
					a[i] + intervals[i] + (( a[i] == 1 ) ? '' : 's');
			}
			*/
		}
	}
	return ret;
}

function getTimeUntil( end ) {

	var _second = 1000;
	var _minute = _second * 60;
	var _hour = _minute * 60;
	var _day = _hour *24

		function showRemaining()
		{
			var now = new Date();
			var distance = end - now;
			var days = Math.floor(distance / _day);
			var hours = Math.floor( (distance % _day ) / _hour );
			var minutes = Math.floor( (distance % _hour) / _minute );
			var seconds = Math.floor( (distance % _minute) / _second );
			var arr = [];
			arr[0] = days;
			arr[1] = hours;
			arr[2] = minutes;
			arr[3] = seconds;
			return arr;
		}
	return showRemaining(); 
}


function tableClicks(event) {
	var $thisButton, $tgt = $(event.target);
	if ($tgt.is('a.button')) {
		if ($tgt.is('a.yes')||$tgt.is('a.no')) {
			handleStarButton($tgt);
		}
		if ($tgt.is('a.left')||$tgt.is('a.right')) {
			var $offset = getIndexOffset();
			handlePaginateButton($tgt, $offset);
		}
		if($tgt.is('#add-commitment')) {
			handleAddCommitment($tgt);
		}
		if($tgt.is('a.show-details')) {
        	handleShowDetails($tgt);
		}
	} 
	if ($tgt.is('div.button.close')) {
		handleCloseButton($tgt);
	}
	if ($tgt.is('div.star.today')) {
		handleStarClick($tgt);     
	}else if ($tgt.has('div.star.today').length > 0) {
		$tgt = $tgt.find('div.star.today');
		handleStarClick($tgt);     
	}else if ($tgt.prev().is('div.star.today')) {
		$tgt = $tgt.prev();
		handleStarClick($tgt);     
	}

}

function handleStarClick(button) {
	var commitmentId = getId(button);
	if( $(button).hasClass('invalid') ){
    	validate(commitmentId);
	} else if( $(button).hasClass('valid') ){
    	invalidate(commitmentId);
	}
}

function handleShowDetails(button) {
    var commitmentId = getId(button); 
	if( $('#stats-'+commitmentId).length > 0 ) {
    	$('#stats-'+commitmentId).slideUp("normal", function() { $(this).remove(); } );
	} else {
		$.getJSON('commitments/stats/'+commitmentId, function(data) {
                var numStarsNeededForGrade = 5-parseInt(data.Commitment.total);

				var statsFirstPart = 
					"<div id='stats-" + commitmentId + 
					"' class='stats' >" + 
					'<h1>' +  
					'<span class="details label">Since </span>' + 
						'<span class="stats">' + 
						data.Commitment.date + 
						'</span>' + 
					'<span class="details label"> you have earned </span>' + 
						'<span class="stats">' + 
						data.Commitment.valid + 
						'</span>' + 
					'<span class="details label"> out of </span>' + 
						'<span class="stats">' + 
						data.Commitment.total + 
						'</span>' + 
					'<span class="details label"> total stars.</span></h1>';

				var statsSecondPart; 

				if( numStarsNeededForGrade <= 0 ) {
					statsSecondPart = '<h1><span class="details label">AVERAGE: </span>' + 
						'<span class="grade">' + 
						data.Commitment.average + 
						'%</span>' + 
					'</h1>';
				} else {
					statsSecondPart = 
						'<h1><span class="details label"> Once you have </span>' + 
						'<span class="stats">' + 
						numStarsNeededForGrade + 
						'</span>' + 
					'<span class="details label"> more stars, you will be given an average for this commitment.</span></h1>';
					'</h1>';
				}

				var stats =$( statsFirstPart + statsSecondPart ).hide(); 
				stats.appendTo($(button).parent()).slideDown();
				});
	}
}

function handleAddCommitment(button) {
	var rule = $('#CommitmentRule');		
	var days = $('#CommitmentDays');
	var formData = $(rule).parents('form').serialize();
	var id = paramsForJs.fb.id;
	$.post('commitments/add', formData, function(data) {
		setIndexAndScroll(0);
		$(rule).val('');
		$(days).val('1');
	}, 'json');
}

function handleCloseButton(button) {
   var commitmentId = getId(button); 
   var commitment = $(button).parent().next().children('a').text();
   var msg = 'Are you sure you want to permanently delete \'' + commitment + '\'';
   var questionDiv = getQuestion(button, msg, function() {
		$.post('commitments/delete/' + commitmentId, null, 
			function(data) {
			// this doesn't work yet - check for equality rules
			// premature optimization isn't necessary :)
				//if( data == 'true' ) {
					setIndexAndScroll(0);
				//}
			}, 
			'json');

   });
}

function getQuestion( button, msg, callback ) {
	if( $('#modal_question').length == 0 ) {
		$("body").append('<div id="modal_question" class="modal"><h1 class="modal">' + msg + '</h1><input type="button" id="modal_no" value="Cancel" /><input type="button" id="modal_yes" value="Yes" /></div>');
	} else {
    	$('#modal_question').removeClass('gone').children('h1.modal').text(msg);
	}
	$.blockUI({ message: $('#modal_question') }); 
	$('#modal_yes').click(function() {
		$.unblockUI(); 
    	$('#modal_question').addClass('gone');
		return callback.call();
	}); 
	$('#modal_no').click(function() { 
		$.unblockUI(); 
    	$('#modal_question').addClass('gone');
		return false; 
	}); 
}

function handlePaginateButton(button, offset) {
    //var offset = (int)paramsForJs.index_offset;
    var grow = 1;
    if (button.is('#leftpage')) {
        offset = offset - grow;
    }
    if (button.is('#rightpage')) {
        offset = offset + grow;
    }
	setIndexAndScroll(offset);
}

function setIndexAndScroll( offset ) {
    var url = paramsForJs.users_index;
//    url = url + '/index/' + offset + ' #the_table';
    url = url + offset + ' #the_table';
    $('#user_index').load(url, {}, function() {
		if( getIndexOffset() < 0 ) {
			$('#rightpage').removeClass('invalid');
		} else {
			$('#rightpage').addClass('invalid');
		}
		$('table').click(tableClicks);
		setupTimers();
		updateTimers();
    });
}

function getIndexOffset() {
    return parseInt($('#the_table').attr('class'));
}

function handleStarButton(button) {
    var commitmentId = getId(button);
	if( $(button).is('a.yes') ) {
		validate(commitmentId);
    }
    if( $(button).is('a.no') ) {
		invalidate(commitmentId);
    }
}

function validate(commitmentId) {
	var needsToMove = ( getIndexOffset() != 0 );
	var lastStar = '#today_commit_' + commitmentId;
	$(lastStar).removeClass('invalid').addClass('valid');
	if( ! needsToMove ) {
    	$('div#time_remaining_'+ commitmentId).addClass('gone');
	}
	$.post(paramsForJs.stars_certify + '/' + commitmentId, function() 
			{
			if( needsToMove ) {setIndexAndScroll(0);}
			return true;
			});
}

function invalidate(commitmentId) {
	var needsToMove = ( getIndexOffset() != 0 );
	var lastStar = '#today_commit_' + commitmentId;
	$(lastStar).removeClass('valid').addClass('invalid');
	if( ! needsToMove ) {
    	$('div#time_remaining_'+ commitmentId).removeClass('gone');
	}
	$.post(paramsForJs.stars_nullify + '/' + commitmentId, function()
			{
			if( needsToMove ) {setIndexAndScroll(0);}
			return true;
			});
}

function getId(target) {
    var query = /\d+/;
    var idStr = $(target).attr('id');
    var result = query.exec(idStr);
	return (result != null) ? parseInt(result[0]) : '';
}

function toggleStar(div) {
    var curStyle = $(div).attr('class');
    var newStyle = (curStyle == 'star invalid') ? 'star valid' : 'star invalid';
    $(div).attr('class',newStyle);
}

/* // old stats message
 *
				var stats = $(
					"<div id='stats-" + commitmentId + 
					"' class='stats' >" + 
					'<h1>' +  
					'<span class="details label">' + 'since: </span><span class="stats">' + 
					data.Commitment.date + '</span>' + 
					'</h1>' + 
					'<h1>' + 
					'<span class="stats fraction">' + 
					data.Commitment.valid + 
					'/' + data.Commitment.total +  
					'</span><span class="stats percent">' + 
					data.Commitment.average + '%</span>' + 
					'</h1>' + 
					'<h1><span class="details label">' + 'grade: </span><span class="grade">' + 
					data.Commitment.grade + '</span>' + 
					'</h1>' + 
					'</div>'
					).hide(); 
 */
