/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
// via http://greatwebguy.com/programming/dom/setting-your-tabindex-on-your-html-forms-automatically-with-jquery/
$(document).ready(function(){
$('form').each(function() {
var tabindex = 1;
$('input').each(function() { // ommitted ,select,textarea
var $input = $(this);
if ($input.is(':visible')) {
$input.attr("tabindex", tabindex);
tabindex++;
}
});
});
});//document.ready
// taken from http://stackoverflow.com/questions/267615/focusing-first-control-in-the-form-using-jquery
$(function () {
  $(window).load(function () {
    $(':input:visible:enabled:first').focus();
  });
})
/*$j(document).ready(function() {
var tabindex = 0; // FYI: Tabindexes starts with 0.
$j(":input[type=text]:visible").each(function(i,e){
$j(e).attr('tabindex',i) ;
});
});*/


