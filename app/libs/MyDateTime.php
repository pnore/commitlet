<?php 


/**
 * Prints out debug information about given variable.
 *
 * Only runs if debug level is greater than zero.
 *
 * @param boolean $var Variable to show debug information for.
 * @param boolean $showHtml If set to true, the method prints the debug data in a screen-friendly way.
 * @param boolean $showFrom If set to true, the method prints from where the function was called.
 * @link http://book.cakephp.org/view/1190/Basic-Debugging
 * @link http://book.cakephp.org/view/1128/debug
 **
function debug($var = false, $showHtml = false, $showFrom = true) {
	if ($showFrom) {
		$calledFrom = debug_backtrace();
		echo '<strong>' . substr(str_replace(ROOT, '', $calledFrom[0]['file']), 1) . '</strong>';
		echo ' (line <strong>' . $calledFrom[0]['line'] . '</strong>)';
	}
	echo "\n<pre class=\"debug\">\n";

	$var = print_r($var, true);
	if ($showHtml) {
		$var = str_replace('<', '&lt;', str_replace('>', '&gt;', $var));
	}
	echo $var . "\n</pre>\n";
}*/

class MyDateInterval /*extends DateInterval*/ {

	var $y;
	var $m;// months
	var $d;
	var $h;
	var $i;// minutes
	var $s;
	var $invert ;
	var $days ;

	/* Methods */
	function __construct ( $interval_spec = null ) {
		if( $interval_spec != null ) {
			$datePos = stripos( $interval_spec, "P" );
			$timePos = stripos( $interval_spec, "T" );
			if( $timePos !== false ) {
				$tStr = substr( $interval_spec, $timePos+1 ); 
				$this->parseArrayToClassVars( 'time', $tStr );
			}
			if( $datePos !== false ) {
				if( $timePos !== false ) {
					$dStr = substr( $interval_spec, 1, $timePos-1 );
				}  else {
					$dStr = substr( $interval_spec, 1 );
				}
				$this->parseArrayToClassVars( 'date', $dStr );
			}
		}
		$this->boundsCheckValues();
	}

	private function parseArrayToClassVars( $type, $st ) {
		$vars = array(
			'date' => array( 
				'Y' => 'y',
				'M' => 'm', 
				'D' => 'd'
			),
			'time' => array( 
				'H' => 'h',
				'M' => 'i', 

				'S' => 's'
			)
		);   
		// turn the string into json
		$members = preg_replace( 
			'/(\d+)([A-Za-z])/', '"${2}":${1}, ', $st );
		// cut final comma
		$members = "{" . substr($members, 0,strlen($members)-2 ).  "}";
		// trim the final comma, encase in brackets, and decode json
		$members = json_decode($members, true);
		foreach( $members as $ident=>$val ) {
			$classVarName = $vars[$type][$ident];
			$this->{$classVarName} = $val;
		}
	}

	private function boundsCheckValues() {
		$boundsCheck = array( 
			'y' => array( 
				'min' => 0,
				'max' => 150
			),
			'm' => array(
				'min' => 0,
				'max' => 12
			), 
			'd' => array(
				'min' => 0,
				'max' => 31
			),
			'h' => array(
				'min' => 0,
				'max' => 24
			), 
			'i' => array( 
				'min' => 0,
				'max' => 59
			),
			's' => array( 
				'min' => 0,
				'max' => 59
			),
			'days' => array(
				'min' => 0,
				'max' => 54750,
				'default' => false
			),
			'invert' => array(
				'min' => 0,
				'max' => 1, 
				'default' => 0
			)
		);   
		foreach( $boundsCheck as $varname => $bounds ) {
			$min = $bounds['min'];
			$max = $bounds['max'];
			// if default is undefined, take min
			$default = (isset($bounds['default'])) 
				? $bounds['default'] : $bounds['min'];
			$v = $this->$varname;
			if( isset($v) ) {
				$v = ($v < $min) ?  $min : ( ($v > $max) ? $max : $v );
			} 
			$this->$varname = (isset($v)) ? $v : $default;
		}	
	}
/*
Y	Years, numeric, at least 2 digits with leading 0	01, 03
y	Years, numeric	1, 3
M	Months, numeric, at least 2 digits with leading 0	01, 03, 12
m	Months, numeric	1, 3, 12
D	Days, numeric, at least 2 digits with leading 0	01, 03, 31
d	Days, numeric	1, 3, 31
a	Total amount of days	4, 18, 8123
H	Hours, numeric, at least 2 digits with leading 0	01, 03, 23
h	Hours, numeric	1, 3, 23
I	Minutes, numeric, at least 2 digits with leading 0	01, 03, 59
i	Minutes, numeric	1, 3, 59
S	Seconds, numeric, at least 2 digits with leading 0	01, 03, 57
s	Seconds, numeric	1, 3, 57
not supported -> R	Sign "-" when negative, "+" when positive	-, +
not supported -> r	Sign "-" when negative, empty when positive	-,
 */
	function format( $format ) {
    	$str = str_replace( '%Y', sprintf( '%02d' , $this->y ) , $format );
    	$str = str_replace( '%y', sprintf( '%2d' , $this->y ) , $str );
    	$str = str_replace( '%M', sprintf( '%02d' , $this->m ) , $str );
    	$str = str_replace( '%m', sprintf( '%2d' , $this->m ) , $str );
    	$str = str_replace( '%D', sprintf( '%02d' , $this->d ) , $str );
    	$str = str_replace( '%d', sprintf( '%2d' , $this->d ) , $str );
    	$str = str_replace( '%a', sprintf( '%d' , $this->days ) , $str );
    	$str = str_replace( '%H', sprintf( '%02d' , $this->h ) , $str );
    	$str = str_replace( '%h', sprintf( '%2d' , $this->h ) , $str );
    	$str = str_replace( '%I', sprintf( '%02d' , $this->i ) , $str );
    	$str = str_replace( '%i', sprintf( '%2d' , $this->i ) , $str );
    	$str = str_replace( '%S', sprintf( '%02d' , $this->s ) , $str );
    	$str = str_replace( '%s', sprintf( '%2d' , $this->s ) , $str );
		return $str;
	}
}

class MyDateTime extends DateTime {

	var $date;

	function __construct($str = null) {
		$vArgs = func_get_args(); // you can't just put func_get_args() into a function as a parameter 
		call_user_func_array(array('parent', '__construct'), $vArgs); 
		$this->updateDate();
	}

	function sub( $i ) {
 		$this->modify( "- $i->y years" );
 		$this->modify( "- $i->m months" );
 		$this->modify( "- $i->d days" );
 		$this->modify( "- $i->i minutes" );
 		$this->modify( "- $i->h hours" );
 		$this->modify( "- $i->s seconds" );
		$this->updateDate();
		return $this;
	}

	private function updateDate() {
		$this->date = parent::format('Y-m-d H:i:s'); 
	}

	function add( $i ) {
 		$this->modify( "+ $i->y years" );
 		$this->modify( "+ $i->m months" );
 		$this->modify( "+ $i->d days" );
 		$this->modify( "+ $i->i minutes" );
 		$this->modify( "+ $i->h hours" );
 		$this->modify( "+ $i->s seconds" );
		$this->updateDate();
		return $this;
	}

	function diff( $i ) {
		$d1 = clone $this;
		$d2 = clone $i;
		$d1ts = $d1->getTimestamp();
		$d2ts = $d2->getTimestamp();
		$daysBetween = $this->daysBetween($d1ts, $d2ts);
		if( $d1ts < $d2ts ) {
			$arr = $this->date_diff_array( $d1, $d2 );
		} else {
			if( $d1ts != $d2ts ) {
				$arr = $this->date_diff_array( $d2, $d1 );
			}   else {
				return new MyDateInterval();
			}
		}
		$dti = new MyDateInterval();
		$dti->y = $arr['year'];
		$dti->m = $arr['month'];
		//$dti->d = ($arr['week']*7) + $arr['day'];
		$dti->d = $arr['day'];
		$dti->h = $arr['hour'];
		$dti->i = $arr['minute'];
		$dti->s = $arr['second'];
		$dti->days = $daysBetween;
		/*
		debug($arr['year'] . ' years');
		debug($arr['month'] . ' months');
		debug($arr['day'] . ' days');
		debug($arr['hour'] . ' hours');
		 */
		return $dti;
	}

	/**
	 * assumes you want to find the number of FULL DAYS BETWEEN the
	 * two dates
	 * both start and end are unix timetamps
	 * http://stackoverflow.com/questions/2040560/how-to-find-number-of-days-between-two-dates-using-php/2040575#2040575
	 **/
	private function daysBetween($start, $end) {
        return (int)floor(abs($end - $start) / 86400);
	}

	/**
	 * http://stackoverflow.com/questions/676824/how-to-calculate-the-date-difference-between-2-dates-using-php/3959860#3959860
	 * @author enobrev
	 * @param DateTime $oDate1
	 * @param DateTime $oDate2
	 * @return array 
	 */
	function date_diff_array(DateTime $oDate1, DateTime $oDate2) {
		$aIntervals = array(
			'year'   => 0,
			'month'  => 0,
		 //   'week'   => 0,
		    'day'    => 0,
		    'hour'   => 0,
		    'minute' => 0,
		    'second' => 0,
		);
		foreach($aIntervals as $sInterval => &$iInterval) {
			while($oDate1->getTimestamp() <= $oDate2->getTimestamp() ){ 
				if( $oDate1->getTimestamp() == $oDate2->getTimestamp() ) {
					//debug('met target');
					break 2;
				} else {
					//debug('did not meet target');
				}
				$oDate1->modify('+1 ' . $sInterval);
				//debug('adding +1 ' . $sInterval . ' is ' . $oDate1->format('Y M j, H:i:s') . ' for total of ' . $iInterval . ' ' . $sInterval .'(s)' );
				if ( $oDate1->getTimestamp() > $oDate2->getTimestamp() ) {
					$oDate1->modify('-1 ' . $sInterval);
					//debug('overshot, subtracting a ' . $sInterval );
					break;
				} else {
					$iInterval++;
				}
			}
		}
		//debug($aIntervals);

		return $aIntervals;
	}

	function getTimestamp() {
		return parent::format('U');
	}	
}

if(!class_exists('MyDatePeriod')):
class MyDatePeriod /*extends MyDatePeriod */ implements Iterator {

	/* Constants */
	const EXCLUDE_START_DATE = 1 ;

	var $curr;
	var $totalNumIntervals;
	var $startDate;
	var $interval;
	var $endDate;
	var $recurrences;
	// true if recurrence mode, false if end date mode
	var $isRecurrenceMode;
	var $valid;

	/**
	 * Note that $start isn't &$start
	 **/
	function __construct ( $start , $interval , $numRecurrencesOrEndDate ){
    	if( $numRecurrencesOrEndDate instanceof DateTime ) {
			//$this->isRecurrenceMode = false;
			$this->isRecurrenceMode = 0;
			$this->endDate = $numRecurrencesOrEndDate;	
		} else {
			//$this->isRecurrenceMode = true;
			$this->isRecurrenceMode = 1;
			$this->totalNumIntervals = $numRecurrencesOrEndDate;	
		}
		if(! $start instanceof DateTime ) throw new InvalidArgumentException( "you must supply a datetime for the first parameter to date period" );
		$this->startDate = $start;
		$this->interval = $interval;
	}

	/**
	function __construct ( string $isostr [, int $options ] ){

	}
	**/

	function rewind() {
		$this->curr = $this->startDate;
		$this->valid = true;
		if( $this->isRecurrenceMode ) {
			$this->recurrences = 0;
		}
	}

	function current() {
		return clone $this->curr;
	}

	function key() {
		return clone $this->curr;
	}

	function next() {
 		$this->curr->add($this->interval);
		$this->recurrences++;
		if( $this->isRecurrenceMode ) {
			if( $this->recurrences >= $this->totalNumIntervals ) {
				$this->valid = false;
			}
		} else {
			if( $this->curr->getTimeStamp() 
				> $this->endDate->getTimeStamp() ) {
				$this->valid = false;
			}
		}
	}

	function valid() {
		return $this->valid;
	}

	function getNumRecurrences() {
    	return $this->recurrences;
	}
}
endif;
/*
 // test of MyDateInterval 
echo "<h1>testing MyDateInterval</h1><br/>"; 
$tests = array( 'P2Y3D', 'P5Y5M3DT3H5M54S');
$formats = array( '%Y years, %M months, %D days, %H hours, %I minutes, %S seconds',
  '%y years, %m months, %d days, %h hours, %i minutes, %s seconds'	);
foreach( $tests as $test ) {
	echo "<p>$test</p>";
	$thisTest = new MyDateInterval( $test );
	debug($thisTest);
	foreach( $formats as $format ) {
		debug($format);
		debug($thisTest->format($format));
	}
	echo "<hr/>";
}
$arr = array( 
	'year' => 2,
	'month' => 4,
	'day' => 25,
	'hour' => 10,
	'minute' => 23
);
$thisTest = new MyDateInterval();
$thisTest->y = $arr['year'];
$thisTest->m = $arr['month'];
$thisTest->d = $arr['day'];
$thisTest->h = $arr['hour'];
$thisTest->m = $arr['minute'];
$thisTest->s = $arr['second'];
debug($thisTest);
foreach( $formats as $format ) {
	debug($format);
	debug($thisTest->format($format));
}
echo "<hr/>";
 */


/*
 // test of MyDateTime
echo "<h1>testing MyDateTime Add</h1><br/>"; 
echo '<style type="text/css">
td {
	border: solid 1px;
	padding: 10px;
}
</style>';
$tests = array( '2010-12-13 16:47:07', '2010-12-05 07:45:41');
$toAdd = array( 'P3DT2H', 'T8H', 'P1Y2M', 'P1YT3H' );
$addFormat = '%y y, %m m, %d d, %h h, %i m, %s s';
$dateFormat = 'Y M j, H:i:s';
echo '<table style="border:solid 1px; padding:10px;"><tr>';
for( $i = 0; $i < count($toAdd); $i++ ) {
	echo "<th>start date " . ($i+1) . "</th><th>adding " . $toAdd[$i] . "</th><th>end date " . ($i+1) . "</th>";
}	

foreach( $tests as $test ) {
	echo "<tr>";
 	for( $i = 0; $i < count($toAdd); $i++ ) {
   		$date = new MyDateTime($test);    		 
		$add = new MyDateInterval($toAdd[$i]);
		echo "<td>" . $date->format($dateFormat)  . "</td>";
   		echo "<td>" . $add->format($addFormat) . "</td>";
		$date->add($add);
		echo "<td>" . $date->format($dateFormat)  . "</td>";
	}	
	echo "</tr>";
}
echo "</table><br/>";
 
echo "<h1>testing MyDateTime Sub</h1><br/>"; 
echo '<style type="text/css">
td {
	border: solid 1px;
	padding: 10px;
}
</style>';
$tests = array( '2010-12-13 16:47:07', '2010-12-05 07:45:41');
$toSub = array( 'P3DT2H', 'T8H', 'P1Y2M', 'P1YT3H' );
$toSubtract = array( 'T8H' );
$subFormat = '%y y, %m m, %d d, %h h, %i m, %s s';
$dateFormat = 'Y M j, H:i:s';
echo '<table style="border:solid 1px; padding: 10px;"><tr>';
for( $i = 0; $i < count($toSub); $i++ ) {
	echo "<th>start date " . ($i+1) . "</th><th>subtracting " . $toSub[$i] . "</th><th>end date " . ($i+1) . "</th>";
}	

foreach( $tests as $test ) {
	echo "<tr>";
 	for( $i = 0; $i < count($toSub); $i++ ) {
   		$date = new MyDateTime($test);    		 
		$sub = new MyDateInterval($toSub[$i]);
		echo "<td>" . $date->format($dateFormat)  . "</td>";
   		echo "<td>" . $sub->format($subFormat) . "</td>";
		$date->sub($sub);
		echo "<td>" . $date->format($dateFormat)  . "</td>";
	}	
	echo "</tr>";
}
echo "</table><br/>";
 */


/*
echo "<h1>testing MyMyDatePeriod</h1><br/>"; 
echo '<style type="text/css">
td {
	border: solid 1px;
	padding: 10px;
}
</style>';
$dates = array( '2010-12-13 16:47:07', '2010-12-05 07:45:41');
$intervals = array( 'P1D', 'T8H', 'P1M');
$ends = array( 5, 3, new MyDateTime('2011-02-01 00:00:00'), new MyDateTime('2010-12-13 16:47:07') );
$intervalFormat = '%y y, %m m, %d d, %h h, %i m, %s s';
$dateFormat = 'Y M j, H:i:s';
echo '<table style="border:solid 1px; padding: 10px;"><tr>';
echo '<th>start</th><th>interval</th><th>end</th><th>list</th><th>recurrences</th>';

foreach( $dates as $date ) {
	foreach( $intervals as $interval ) {
		foreach( $ends as $end ) {
			$startDate = new MyDateTime($date);
			$int = new MyDateInterval($interval);
			$period = new MyDatePeriod( $startDate, $int, $end );
			echo "<tr>";
			echo "<td>" . $startDate->format($dateFormat) . "</td>";
			echo "<td>" . $int->format($intervalFormat) . "</td>";
			echo "<td>";
			if( $end instanceof DateTime ) {
            	echo $end->format($dateFormat);	
			} else {
                echo $end;
			}
			echo "</td>";
			echo "<td>";
			foreach( $period as $element ) {
            	echo $element->format($dateFormat) .  "<br/>";
			}
			echo "</td>";
			echo "<td>" . $period->getNumRecurrences() . "</td>";
			echo "</tr>";
		}
	}
	echo "<tr>";
 	for( $i = 0; $i < count($toSub); $i++ ) {
   		$date = new MyDateTime($test);    		 
		$sub = new MyDateInterval($toSub[$i]);
		echo "<td>" . $date->format($dateFormat)  . "</td>";
   		echo "<td>" . $sub->format($subFormat) . "</td>";
		$date->sub($sub);
		echo "<td>" . $date->format($dateFormat)  . "</td>";
	}	
	echo "</tr>";
}
echo "</table><br/>";
 */

/*
echo "<h1>testing MyDateTime->diff()</h1><br/>"; 
echo '<style type="text/css">
td {
	border: solid 1px;
	padding: 10px;
}
</style>';
$dates = array( '2010-12-13 16:47:07', '2010-10-05 07:45:41','2009-12-13 16:47:07', '2010-07-05 07:45:41');
$intervalFormat = '%y y, %m m, %d d, %h h, %i m, %s s';
$dateFormat = 'Y M j, H:i:s';
echo '<table style="border:solid 1px; padding: 10px;"><tr>';
echo '<th>start</th><th>end</th><th>difference</th>';

$numDates = count($dates);
for( $i = 0; $i < $numDates-1; $i++ ) {
	$startDate = new MyDateTime($dates[$i]);
	for( $j = $i+1; $j < $numDates; $j++) {
		$endDate = new MyDateTime($dates[$j]);
		echo "<tr>";
		echo "<td>" . $startDate->format($dateFormat) . "</td>";
		echo "<td>" . $endDate->format($dateFormat) . "</td>";
		echo "<td>";
		$myDateInterval = $startDate->diff($endDate);
        echo $myDateInterval->format($intervalFormat);
		echo "</td></tr>";
	}
}
echo "</table><br/>";
 */
?>
