<div class="stars view">
<h2><?php  __('Star');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $star['Star']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Commitment'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($star['Commitment']['statement'], array('controller' => 'commitments', 'action' => 'view', $star['Commitment']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Creation'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $star['Star']['creation']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Note'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $star['Star']['note']; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Star', true), array('action' => 'edit', $star['Star']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Star', true), array('action' => 'delete', $star['Star']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $star['Star']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Stars', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Star', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Commitments', true), array('controller' => 'commitments', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Commitment', true), array('controller' => 'commitments', 'action' => 'add')); ?> </li>
	</ul>
</div>
