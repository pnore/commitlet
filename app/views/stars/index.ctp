<div class="stars index">
	<h2><?php __('Stars');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('commitment_id');?></th>
			<th><?php echo $this->Paginator->sort('creation');?></th>
			<th><?php echo $this->Paginator->sort('note');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($stars as $star):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $star['Star']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($star['Commitment']['statement'], array('controller' => 'commitments', 'action' => 'view', $star['Commitment']['id'])); ?>
		</td>
		<td><?php echo $star['Star']['creation']; ?>&nbsp;</td>
		<td><?php echo $star['Star']['note']; ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $star['Star']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $star['Star']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $star['Star']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $star['Star']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Star', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Commitments', true), array('controller' => 'commitments', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Commitment', true), array('controller' => 'commitments', 'action' => 'add')); ?> </li>
	</ul>
</div>