<div class="stars form">
<?php echo $this->Form->create('Star');?>
	<fieldset>
 		<legend><?php __('Edit Star'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('commitment_id');
		echo $this->Form->input('creation');
		echo $this->Form->input('note');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('Star.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('Star.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Stars', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Commitments', true), array('controller' => 'commitments', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Commitment', true), array('controller' => 'commitments', 'action' => 'add')); ?> </li>
	</ul>
</div>