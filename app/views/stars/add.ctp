<div class="stars form">
<?php echo $this->Form->create('Star');?>
	<fieldset>
 		<legend><?php __('Add Star'); ?></legend>
	<?php
		echo $this->Form->input('commitment_id');
		echo $this->Form->input('creation');
		echo $this->Form->input('note');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Stars', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Commitments', true), array('controller' => 'commitments', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Commitment', true), array('controller' => 'commitments', 'action' => 'add')); ?> </li>
	</ul>
</div>