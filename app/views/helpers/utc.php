<?php

class UtcHelper extends AppHelper {
    
    /**
     * Offsets a given MyDateTime by a given number of hours.  
     * param <MyDateTime> $utcMyDateTime - MyDateTime object representing the time to offset - passed by reference
     * param <int> userOffset - the offset from UTC to displace, for example -5 or 7
     * param <string> format - a string to create a date. see http://php.net/manual/en/function.date.php
     **/
    function offset(&$utcMyDateTime, $userOffset) {
        if(! $utcMyDateTime instanceof MyDateTime ) {
            throw new InvalidArgumentException( 
                "You must supply a MyDateTime object for the first parameter to utcToMyDateTime");
        }
        $interval = new MyDateInterval("PT".abs($userOffset)."H");
        if( $userOffset > 0 ) {
            $utcMyDateTime->add($interval);
        } else if( $userOffset < 0 ) {
            $utcMyDateTime->sub($interval);
        }
    }

}
