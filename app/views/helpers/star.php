<?php
class StarHelper extends AppHelper {
    
    var $flipflop;

    private function getDaysPerStar( $stars, $daysPerStar = null) {
        if( $daysPerStar == null ) {
            if( count($stars) > 0 ) {
                $interval = 
                $stars[0]['Star']['from']->diff($stars[0]['Star']['to']);
				debug($interval->days);
                $daysPerStar = $interval->days;
            } else {
                $daysPerStar = 1;
            }
        } 
        return $daysPerStar;
    }

	function getBlankStars($period) {
		//$class = $this->flip();
		$s = "";
		foreach( $period as $day ) {
		 $s.="<td><div class='star neutral'></div></td>";
		}
		return $s;
	}

    /**
     * Returns all the formatted <td> cells for one row of commitment
     * table.
     **/
    function td($stars, $period, $daysPerStar = null, $offset) {
        $daysPerStar = $this->getDaysPerStar($stars, $daysPerStar);
		debug($daysPerStar);
        $s = '';       
        $padding = count($period) - (count($stars) * $daysPerStar);
        $class = null;
		$oneDayOfSeconds = 24*60*60;
		if( count($stars) < 1 ) {
        	$s = $this->getBlankStars($period, $daysPerStar); 
		}           
		$timestampLeft = new MyDateTime($period[0]->format(AppModel::ISO_DATE));
		$timestampLeft = $timestampLeft->getTimestamp();
		$timestampRight = new MyDateTime($period[count($period)-1]->format(AppModel::ISO_DATE));
		$timestampRight->add(new MyDateInterval('P1D'));
		$timestampRight = $timestampRight->getTimestamp();
		$numStars = 0;
		$testForLong = false;
        foreach($stars as $star) {
			++$numStars;
			$lastStar = ( (count($stars)-$numStars)==0 && $offset==0 ) ;
            $hasMatch = false;
            $startMidnight = $star['Star']['from'];
			$startMidnight = $startMidnight->getTimestamp();
            $endMidnight = $star['Star']['to'];
			$endMidnight = $endMidnight->getTimestamp();
			$stradlesLeft = false;
			$stradlesRight = false;
			// iterate through period until period date 
			// lines up with star date
            while((! $hasMatch) && count($period)>0) {
                $dateMidnight = new MyDateTime($period[0]->format(AppModel::ISO_DATE));
				$dateMidnight = $dateMidnight->getTimestamp();
				if($endMidnight >= $timestampRight){
					$stradlesRight = true;	
				}
				if( $startMidnight <= $dateMidnight && $dateMidnight <= $endMidnight ) {
					$hasMatch = true;
				   if( $startMidnight < $timestampLeft) {
						$stradlesLeft = true;
				   }
                } else {
                    $s.= "<td><div class='star neutral'></div></td>";
                    array_shift($period);
                }
            }
            $slotsLeft = count($period);
			if( $stradlesLeft && $stradlesRight ) {
				//nothing; $slotsLeft = count($period);
			} else if( $stradlesLeft ) {
            	$slotsLeft = ($endMidnight-$timestampLeft)/$oneDayOfSeconds;
			} else if( $stradlesRight ) {
            	$slotsLeft = ($timestampRight-$dateMidnight)/$oneDayOfSeconds;
			}
			if( $stradlesLeft || $stradlesRight ) {
				// number of seconds between the "from" date of the star
				// and the beginning of the interval, divided by num
				// seconds in day 
            	//$slotsLeft = ($endMidnight-$dateMidnight)/$oneDayOfSeconds;
			}
            $slotsLeft = ($slotsLeft < $daysPerStar) ? $slotsLeft : $daysPerStar;
            $s.=$this->getStar($star,$slotsLeft,$lastStar);
            for( $days = 0; $days < $slotsLeft; $days++ ) {
                array_shift($period);
            }
			if( $star['Star']['commitment_id'] == 69 ) {
            	$testForLong = true;	
			}
        }
        return $s;
    }

    function getStar($star, $columns, $isLastStar) {
        $s = '';
        //$class = $this->flip();
		$isToday = false;
        $timeRemaining = 
            $star['Star']['from']->diff(AppModel::$userNow);
        $future = ($timeRemaining->invert == 1);
        $valid = $star['Star']['valid'];
        if( $future && $valid ) { // if today's has been validated
            $type = 'valid';
        } else {
            $type = ($future) ? 'neutral' : (
                ($valid) ? 'valid' : 'invalid'
            );
			debug($type);
			if( $timeRemaining->d < 1 ) {
            	$isToday = true;	
			}
        }
		$today = null;
		if( $isLastStar ) {
        	$class =  'class="alt"';
			$id = $star['Star']['commitment_id'];
			$id = "id='today_commit_$id'";
			$today = 'today';
		} else {
            $class = null;
			$id = $star['Star']['id'];
			$id = "id='star_$id'";
		}
        $s.="<td $class colspan='$columns'>";
        $date = $star['Star']['from']->format(AppController::USER_SHORT_DATE);
		$s.="<div class='contain'>";
		$s.= "<div $id class='star $type $today'>"; 
		$s.="</div>";
		if( $today ) {
			$s.= "<div id='time_remaining_";
			$s.=$star['Star']['commitment_id'];
			$s.="' class='time-remaining gone' ";
			$s.="due='". $star['Star']['to']->format('F d, Y') . "' ";
			$s.="</div>";
		}
		$s.="</div><!-- class=contain -->";
        $s.= "</td>";
        return $s;
    }

    function flip($str = 'class="alt"') {
        $this->flipflop += 1;
        if($this->flipflop % 2 == 0) {
            return $str;
        } 
        return null;
    }

    function th($period) { 
        $s = '';
        foreach($period as $date){
            $datestr = $date->format(AppController::USER_SHORT_DATE);
            $diff = AppModel::$userNow->diff(new MyDateTime($datestr));
            if( $diff->days == 0 ) { $datestr = 'Today'; }
            if( $diff->days == 1 ) { $datestr = 'Yesterday'; }
            $s.="<th>$datestr</th>";
        }
        return $s;
    }

    function getMyDatePeriod($stars) {
        $from = $stars[0]['Star']['from'];
        $to = $stars[count($stars)-1]['Star']['to'];
        return new MyDatePeriod($from,new MyDateInterval('P1D'),$to);
    }

}
