<?php
$paramsForJs = json_encode($paramsForJs);
echo $html->scriptBlock("var paramsForJs = eval('(' + '$paramsForJs' + ')')", array('safe' => true, 'inline' => false));
?>

<?php $this->Html->scriptStart(array('inline' => false)); ?>
$(function() { 
    $('div[title]').tooltip({ position: 'top right' });
});
<?php $this->Html->scriptEnd(); ?>

<?php $this->Html->script('views/users/index.js', array('inline' => false)); ?>
<div id='user_index' class="users index">
<table id='the_table' class="<?php echo $index_offset?>" cellpadding="0" cellspacing="0">
<?php
 //   $paramsForJs = json_encode($paramsForJs);
 //   echo $html->scriptBlock("var paramsForJs = eval('(' + '$paramsForJs' + ')')", array('safe' => true, 'inline' => true));
   $paginationButtonHeight = 64*count($commitments);
?>
    <tr class="header">
<?php 
	for( $j = 0; $j < 3; $j++ ) {
		echo '<th></th>';
	}
   // paginate left
   echo '<th></th>';
	if( isset($period) ) { 
		echo $star->th($period);
	} else {
		for( $j = 0; $j < Star::PAST_SEEK + 1; $j++ ) {
        	echo '<th></th>';
		}
	}
   // paginate right
   echo '<th></th>';
?>
	</tr>
<?php
   $i = 0;
   if( isset($commitments) ) {
	foreach ($commitments as $commitment):
        $id = $commitment['Commitment']['id'];
        $question = $commitment['Commitment']['question'];
		$class = null;
		if ($i % 2 == 0) {
			$class = 'altrow';
		}
?>
	<tr class="<?php echo $class;?>">

		<td class='left-border'><div id='delete_<?php echo $id; ?>' class='button close' title='Delete this commitment'></div></td>

		<td class='header'><?php 
		echo $html->link($question, 
			'javascript:void(0);',
			array( 
				'id'=>"commitment-question-".$id,
	   			'class'=>"show-details button",	
				'title'=>"Show or hide details about this commitment"
			)
			//array('controller'=>'commitments','action'=>'view',$id)
		); 
?>&nbsp;</td>

		<td class="header actions">
<?php 
        echo $this->Html->link(__('Yes', true), 'javascript:void(0);',array('id'=>"stars-certify-".$id, 'class'=>'button yes') ); 
        echo $this->Html->link(__('No', true), 'javascript:void(0);',array('id'=>"stars-nullify-".$id, 'class'=>'button no') ); 
?>
		</td>
<?php 
		if( $i == 0 ) {
			echo "<td class='header actions pagination' rowspan='"
				. count($commitments) . "'>" .
		    $this->Html->link(__(' ', true), 'javascript:void(0);',array('id'=>"leftpage", 'class'=>'button left pagination','style'=>"height:".$paginationButtonHeight."px;" ) )
	 			. "</td>";
		}
?>
		<?php echo $star->td($stars[$id],$period, null, $index_offset); ?>
		<?php 
		if( $i == 0 ) {
			echo "<td class='header actions pagination' rowspan='"
				. count($commitments) . "'>" .
				$this->Html->link(__(' ', true), 'javascript:void(0);',array('id'=>"rightpage", 'class'=>'button right pagination invalid', 
		   'style'=>"height:".$paginationButtonHeight."px;" ) )
	 			. "</td>";
		}
		?>
	</tr>
<?php $i++; endforeach;} ?>
	<tr>
		<td class='left-border' colspan='3'>
<?php
		echo $this->Html->image('new_commitment_128.png', array(
			'alt'=>'Create New Commitment',
			'style'=>'float:left'
			));
?>
<div class='description list'><p><h1 class='modal'>For Example:</h1><ul>
	<li>Every <strong>2</strong> day(s) I will have <br/><strong>gone running</strong>.</li>
	<li>Every <strong>7</strong> day(s) I will have <br/><strong>watched less than two hours of television</strong>.</li>
	<li>Every <strong>3</strong> day(s) I will have <br/><strong>read to the kids</strong>.</li>
</ul></div>
		</td>
		<td id='add_commitment_td' class='header' colspan='6'>
<?php 
   	echo $this->Form->create('Commitment', array(
				'default'=>false
			));
    echo $this->Form->input('Commitment.days', array(
        'label'=>array('text'=>"Starting today, I'll sign in every ",
        'class'=>'commitment',
    ),
        'div'=>'form-phrase',
		'after'=>' day(s), <br/>',
        'class' => 'commitment days',
        'value'=>1
    ));
	echo $this->Form->input('Commitment.rule', array(
		'label'=> array( 'text'=>'to verify that I have ', 
			'class' => 'commitment rule',
		), 
		'after'=>'.',
		'div'=>'form-phrase',
		'class'=>'commitment rule'
	));
	echo $this->Form->end();
?>
		</td>
		<td class="header actions" colspan="4">
<?php        
	echo $this->Html->link(
		__('Commit', true), 
		'javascript:void(0);',
		array('id'=>"add-commitment", 'class'=>'button commit') 
	); 
?>
		</td>
    </tr>
	</table>
</div>
<div id="modal_question" class="modal gone"><h1 class="modal"></h1><input type="button" id="modal_no" value="Cancel" /><input type="button" id="modal_yes" value="Yes" /></div>
