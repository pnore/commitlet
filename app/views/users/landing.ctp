<h1 class='frontpage section'><?php e($html->image('icon_106.jpg', array('alt'=>'Commitlet icon', 'class'=>'main-icon', 'rel'=>'image_src'))); ?><span class='site-name first-letter'>C</span><span class='site-name not-first-letter'>ommitlet is just ...</span></h1>
<div class='frontpage clear'>
	<p>
... a site that helps you meet your daily commitments. Whether you have a New Year's resolution or just want to remember to water your plants, Commitlet gives you a painless, brainless way to see whether you have actually done what you intended to do.
   </p>
<?php 
echo $html->image('sample_commitment_table.png', array(
	'alt'=>'Sample use of commitlet',
	'class'=>'frontpage',
	'id'=>'sample-use-1'
));
?>
		<p>
Your first time at Commitlet you are asked to write down a commitment. Each day, you have the opportunity to give yourself a “gold star” for that commitment if you’ve lived up to your statement. You’re on the honor system, but after midnight, your opportunity to get credit for that day has passed. As time passes, you can see a record of gold stars for all your commitments. 
		</p>
</div>
<h1 class='frontpage section'>"Know thyself ..."</h1>
<div class='frontpage clear'>
	<p>
	As <?php echo $html->link('Greek philosophers', 'http://en.wikipedia.org/wiki/Know_thyself', array('target'=>'_blank')); ?> realized, one of the greatest challenges is knowing what kind of person you really are. The goal of Commitlet is to make it easier to see how you're doing on the things that you're committed to.
	</p>
<?php 
echo $html->image('sample_statistics_1.png', array(
	'alt'=>'Sample Commitlet statistics',
	'class'=>'frontpage',
	'id'=>'sample-use-2'
));
?>
</div>
<h1 class='frontpage section'>Inspiration</h1>
<div class='frontpage clear'>
	<p>
The inspiration for Commitlet is a TED talk by Derek Sivers, in which he presents research that suggests that people are more likely to reach their goals if they hold off telling people about them until after they’ve done the hard work of making them come about. While Commitlet uses Facebook to login, it doesn’t publish your goals - in fact, you are encouraged to keep your commitments to yourself. Your privacy is of the highest importance - there are even plans to encrypt your commitments before they travel through the internet. 
	</p>
 <!--copy and paste--><object width="446" height="326"><param name="movie" value="http://video.ted.com/assets/player/swf/EmbedPlayer.swf"></param><param name="allowFullScreen" value="true" /><param name="allowScriptAccess" value="always"/><param name="wmode" value="transparent"></param><param name="bgColor" value="#ffffff"></param> <param name="flashvars" value="vu=http://video.ted.com/talks/dynamic/DerekSivers_2010G-medium.flv&su=http://images.ted.com/images/ted/tedindex/embed-posters/DerekSivers-2010G.embed_thumbnail.jpg&vw=432&vh=240&ap=0&ti=947&introDuration=15330&adDuration=4000&postAdDuration=830&adKeys=talk=derek_sivers_keep_your_goals_to_yourself;year=2010;theme=how_we_learn;theme=new_on_ted_com;theme=how_the_mind_works;theme=the_creative_spark;theme=unconventional_explanations;theme=a_taste_of_tedglobal_2010;event=TEDGlobal+2010;&preAdTag=tconf.ted/embed;tile=1;sz=512x288;" /><embed src="http://video.ted.com/assets/player/swf/EmbedPlayer.swf" pluginspace="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" wmode="transparent" bgColor="#ffffff" width="446" height="326" allowFullScreen="true" allowScriptAccess="always" flashvars="vu=http://video.ted.com/talks/dynamic/DerekSivers_2010G-medium.flv&su=http://images.ted.com/images/ted/tedindex/embed-posters/DerekSivers-2010G.embed_thumbnail.jpg&vw=432&vh=240&ap=0&ti=947&introDuration=15330&adDuration=4000&postAdDuration=830&adKeys=talk=derek_sivers_keep_your_goals_to_yourself;year=2010;theme=how_we_learn;theme=new_on_ted_com;theme=how_the_mind_works;theme=the_creative_spark;theme=unconventional_explanations;theme=a_taste_of_tedglobal_2010;event=TEDGlobal+2010;"></embed></object>
</div>
<h1 class='frontpage section'>Comments</h1>
<div id='fb-comments' class='frontpage clear'>
<?php
	echo $facebook->comments();
?>
</div>
<h1 class='frontpage section'>Credits</h1>
<div class='frontpage clear'>
	<p>
Commitlet was designed and developed by Peter Nore. The main font is <?php echo $html->link('Delicious Heavy by Jos Buivenga', 'http://www.josbuivenga.demon.nl/delicious.html', array('target'=>'_blank'));?>, licensed under the <?php echo $html->link('exljbris Font Foundry Free Font License Agreement', 'http://www.josbuivenga.demon.nl/eula.html', array('target'=>'_blank'));?>. The icons are from the excellent <?php echo $html->link('Token icon set by Evan Brooks', 'http://brsev.com/token.html#item', array('target'=>'_blank'));?>, licensed under <?php echo $html->link('Creative Commons attribution-noncommercial-share-alike 3.0', 'http://creativecommons.org/licenses/by-nc-sa/3.0/', array('target'=>'_blank'));?>. The site was built using a <?php echo $html->link('LAMP stack', 'http://en.wikipedia.org/wiki/LAMP_(software_bundle)', array('target'=>'_blank'));?>, the <?php echo $html->link('CakePHP framework', 'http://cakephp.org/', array('target'=>'_blank'));?>, the <?php echo $html->link('Facebook SDK', 'http://developers.facebook.com/docs/reference/javascript/', array('target'=>'_blank'));?>, and the <?php echo $html->link('jQuery javascript framework', 'http://jquery.com', array('target'=>'_blank'));?>.
	</p>
</div>
<div id='fb'>
<?php
	echo $facebook->like();
?>
</div>
