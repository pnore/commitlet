<div class="commitments index">
	<h2><?php __('Commitments');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('facebook_id');?></th>
			<th><?php echo $this->Paginator->sort('statement');?></th>
			<th><?php echo $this->Paginator->sort('days');?></th>
			<th><?php echo $this->Paginator->sort('rule');?></th>
			<th><?php echo $this->Paginator->sort('question');?></th>
			<th><?php echo $this->Paginator->sort('start_date');?></th>
			<th><?php echo $this->Paginator->sort('modified');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
    debug($commitments);
	$i = 0;
	foreach ($commitments as $commitment):
        /*
        $modifiedDateString = $commitment['Commitment']['modified'];
        $dateTimeObject = new MyDateTime($modifiedDateString);
        $utc->offset( $dateTimeObject, $fb['timezone'] );
        $modified = $dateTimeObject->format($userMyDateTimeString);
         */

        $note = (isset($commitment['Note'][0]['text']))
            ? $commitment['Note'][0]['text'] : '';
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $html->link($commitment['Commitment']['id'], array( 'controller'=>'commitments', 'action'=>'view', $commitment['Commitment']['id'])); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($fb['name'], array('controller' => 'users', 'action' => 'view', $fb['id'])); ?>
		</td>
        
		<td><?php echo $note; ?>&nbsp;</td>
		<td><?php echo $commitment['Commitment']['days']; ?>&nbsp;</td>
		<td><?php echo $commitment['Commitment']['rule']; ?>&nbsp;</td>
		<td><?php echo $commitment['Commitment']['question']; ?>&nbsp;</td>
		<td><?php echo $commitment['Commitment']['start_date']; ?>&nbsp;</td>
		<td><?php echo $commitment['Commitment']['modified']->format($userMyDateTimeString); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $commitment['Commitment']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $commitment['Commitment']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $commitment['Commitment']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $commitment['Commitment']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Commitment', true), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Users', true), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User', true), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Stars', true), array('controller' => 'stars', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Star', true), array('controller' => 'stars', 'action' => 'add')); ?> </li>
	</ul>
</div>
