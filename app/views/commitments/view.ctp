<div class="commitments view">
<h2><?php  __('Commitment');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $commitment['Commitment']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('User'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $this->Html->link($fb['name'], array('controller' => 'users', 'action' => 'view', $fb['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Statement'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
            <?php $note = (isset($commitment['Note'][0]['text']))?$commitment['Note'][0]['text']:'';
                echo $note; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Rule'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $commitment['Commitment']['rule']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Days'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $commitment['Commitment']['days']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Promise'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $commitment['Commitment']['question']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $commitment['Commitment']['start_date']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>><?php __('Modified'); ?></dt>
<?php
        $modifiedDateString = $commitment['Commitment']['modified'];
        $dateTimeObject = new MyDateTime($modifiedDateString);
        $utc->offset( $dateTimeObject, $fb['timezone'] );
        $modified = $dateTimeObject->format($userMyDateTimeString);
?>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $modified; ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Commitment', true), array('action' => 'edit', $commitment['Commitment']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Commitment', true), array('action' => 'delete', $commitment['Commitment']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $commitment['Commitment']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Commitments', true), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Commitment', true), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users', true), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User', true), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Stars', true), array('controller' => 'stars', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Star', true), array('controller' => 'stars', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php __('Related Stars');?></h3>
	<?php if (!empty($commitment['Star'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php __('Id'); ?></th>
		<th><?php __('Commitment Id'); ?></th>
		<th><?php __('Creation'); ?></th>
		<th><?php __('Note'); ?></th>
		<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($commitment['Star'] as $star):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $star['id'];?></td>
			<td><?php echo $star['commitment_id'];?></td>
			<td><?php echo $star['creation'];?></td>
			<td><?php echo $star['note'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'stars', 'action' => 'view', $star['id'])); ?>
				<?php echo $this->Html->link(__('Edit', true), array('controller' => 'stars', 'action' => 'edit', $star['id'])); ?>
				<?php echo $this->Html->link(__('Delete', true), array('controller' => 'stars', 'action' => 'delete', $star['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $star['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Star', true), array('controller' => 'stars', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
