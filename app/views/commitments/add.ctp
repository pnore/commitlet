<div class="commitments form">
<?php  
echo $this->Form->create('Commitment');?>
	<fieldset>
 		<legend><?php __('New Commitment'); ?></legend>
	<?php
		echo $this->Form->hidden('Commitment.facebook_id', array('value'=>$fb['id']));
        //echo $this->Form->input('user_id', array('value' => ) );
echo $this->Form->input('Note.0.text', array(
    'label' => 'What sort of commitment do you want to make? Why?'
    ));
    echo $this->Form->input('Commitment.start_date', array(
        'label'=> array( 'text'=>'Starting on ',
            'class' => 'commitment date',
        ),
        'div'=>'form-phrase',
        'class' => 'commitment date'
    ));
    echo $this->Form->input('Commitment.days', array(
        'label'=>array('text'=>", I'll sign in every ",
        'class'=>'commitment',
    ),
        'div'=>'form-phrase',
        'class' => 'commitment days',
        'value'=>1
    ));
    echo $this->Form->input('Commitment.rule', array(
        'label'=> array( 'text'=>' day(s) to verify that I have ',
        'class' => 'commitment rule',
    ),
        'div'=>'form-phrase', 
        'after'=> '.',
        'class' => 'commitment rule'
    ));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Commit', true));?>
</div>
