<div class="commitments form">
<?php echo $this->Form->create('Commitment');?>
	<fieldset>
 		<legend><?php __('Edit Commitment'); ?></legend>
	<?php
        echo $this->Form->hidden('Commitment.facebook_id', array('value'=>$fb['id']));
        echo $this->Form->input('Commitment.statement', array(
            'label' => 'What sort of commitment do you want to make? Why?'
        ));
        echo $this->Form->input('Commitment.days', array(
            'label'=>array('text'=>"In order to make a commitment, it helps be clear. Write a <strong>yes or no question</strong> that best defines your commitment:<br/><br/> In the last",
            'class'=>'commitment',
            ),
            'div'=>'form-phrase',
            'after' => 'day(s), have I', 'class'=>'commitment',
            'class' => 'commitment days',
            'value'=>1
        ));
        echo $this->Form->input('Commitment.rule', array(
                'label'=> array( 'text'=>'',
                'class' => 'commitment rule',
            ),
            'div'=>'form-phrase', 
            'after'=> '<strong class="big">?</strong>',
            'class' => 'commitment rule'
        ));  
        /*
		echo $this->Form->input('id');
		echo $this->Form->input('user_id');
		echo $this->Form->input('statement');
		echo $this->Form->input('rule');
         */
	?>
	</fieldset>
<?php echo $this->Form->end(__('Recommit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('Commitment.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('Commitment.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Commitments', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Users', true), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User', true), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Stars', true), array('controller' => 'stars', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Star', true), array('controller' => 'stars', 'action' => 'add')); ?> </li>
	</ul>
</div>
