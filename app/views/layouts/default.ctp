<!DOCTYPE html>
<?php echo $facebook->html(); ?>
    <head>
        <?php echo $this->Html->charset(); ?>
        <title><?php __('Commitlet'); ?></title>
        <meta property="og:title" content="Commitlet"/>
        <meta property="og:type" content="non_profit"/>
        <meta property="og:url" content="http://commitlet.com"/>
        <meta property="og:image" content="http://commitlet.com/img/icon_106.jpg"/>
        <meta property="og:site_name" content="Commitlet"/>
        <meta property="fb:admins" content="100001839062818"/>
        <meta property="og:description"
          content="... a site that helps you meet your daily commitments. Whether you have a New Year's resolution or just want to remember to water your plants, Commitlet gives you a painless, brainless way to see whether you have actually done what you intended to do."/>
        <?php echo $this->Html->meta('icon'); ?>
        
        <?php echo $this->Html->script(array('http://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js')); ?>
        <script>
            WebFont.load({
                custom: { families: ['DeliciousHeavy'], urls: ['/css/type.css'] }
            });
        </script>
        
		<?php echo $this->Html->css(array('cake.generic', 'main'/*, 'type'*/)); ?>
        
        <?php echo $this->Html->script(array('http://cdn.jquerytools.org/1.2.5/jquery.tools.min.js', 'jquery.blockUI')); ?>
        
        <?php echo $scripts_for_layout; ?>
        
        </head>
        <body>
            <div id="container">
                <div id="content">
                    <?php echo $this->Session->flash(); ?>
                    <h1 id='site-name'>Commitlet</h1>
                    <div id="login">
                    <?php if( $fb ) : ?>
                        <?php echo $facebook->picture($fb['id'], array('size'=>'square')); ?>
                        <span id='greeting'>Welcome back, </span><span id='username'><?php echo $fb['first_name']; ?></span>
                        <fb:login-button autologoutlink="true" onlogin="window.location.reload();"></fb:login-button>
                    <?php else: ?>
                        <?php echo $this->Html->image('blank_fb.gif', array('class'=> 'blank_profile', 'alt'=>'profile pic')); ?>
                        <span id='greeting'>Sign in with </span><span id='username'>facebook</span>
                        <?php echo $facebook->login(); ?>
                    <?php endif; ?>
                    </div><div id='header-break' class='clear'></div>
                    
                    <?php echo $content_for_layout; ?>
                    
            </div>
            <div id="footer">
				<ul>
					<li><?php e($html->link('Stars','/')); ?></li>
					<li><?php e($html->link('About','/users/landing')); ?></li>
					<li><?php e($this->Html->link(__('Contact', true), '/users/landing#fb-comments',
					array('id'=>"contact", 'class'=>'actions contact' ) ) ); ?></li>
					<li><?php e($html->link('Peter Nore','javascript:void(0);')); ?></li>
				 </ul>
            </div>
        </div>
            <?php echo $facebook->init(); ?>
            <?php echo $this->Js->writeBuffer(); ?>
    </body>
</html>
