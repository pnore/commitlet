<?php
$readme = array(
    '<h2>Description</h2>' =>
    array('This site is a basic demonstration of using CakePHP, JQuery, and Ajax/JSON to access a remote API - in this case, the Google Maps API and the Bay Area Rapid Transit Company API.'),
    '<h2>Attribution</h2>' => array(
        'Author - Peter Nore'
    ),
    '<h2>Implementation Decisions</h2>' => array(
        "While it would have been pretty easy to write a couple of quick scripts to hook up to the Bart API, I decided to write an entire CakePHP plugin that would take care of rendering the BART API's RESTful interface locally.  Each method in the BART api has it's analog in my plugin that renders the remote data into an array that's appropriate for saving in a database, and implements caching of that array for each type of query. As a CakePHP plugin, anyone who wants to write an application that uses the Bart API only has to drop my plugin into their plugins directory, and the entire interface is exposed to their application."
        . " I decided to make a plugin because I wanted to be able to have the flexibility to expose my own derivative RESTful API should I want to. The plugin caches the formatted arrays because it makes it easier to render the databases, reducing the strain on BART's servers. Incidentally, the caching feature of the plugin aids in "
        . " the development of applications because it makes it easy to develop offline once the files are cached. "
        . " The plugin caches in JSON, and if I ever decided to expose a JSON REST API for BART, it would be pretty fast to render because of the caching.",
        "While the groundwork is laid to do a lot of interesting things with the data, I decided to take a one-page approach. Because each action that renders JSON can also render a view, it would be trivial to implement permalink features and the ability to click on an anchor to add or remove elements from the page."
    ),
    '<h2>Validation and Testing</h2>' => array(
        'All pages validated XHTML 1.0 Transitional at ' . $html->link('the w3c validator service', 'http://validator.w3.org/check'),
        "All pages were tested for consistent functionality and appearance in Firefox 3.6, Chrome 6, and Opera 10 on Linux Ubuntu 10.10, and IE 8 on Windows XP. IE8 has troules rendering the route lines for reasons I couldn't decipher (I'm not very good with the IE debugger yet)."
    ),
    '<h2>Relevant Application Files</h2>' => array(
        'app folder' => array(
            'config folder' => array(
                'core.php - contains core configuration values such as the salt for the hash, the debug level, etc',
                "database.php - contains the MySQL database configuration parameters and my Bart plugin's required API key",
                'routes.php contains the custom rules for routing input urls to controllers in the application'
            ),
            'controllers folder' => array(
                'pages_controller.php - serves up static pages such as this one',
                'routes_controller.php - routes the requests for bart routes',
                'stations_controller.php - routes the requests for bart stations',
                'app_controller.php - sets the access rules for whole application',
            ),
            'models folder' => array(
                'route.php - implements all the business logic involving routes',
                'station.php - implements all the business logic involving stations',
                'trip_step.php - implements all the business logic involving trip_steps, which are the join table for routes and stations',
                'app_model.php - sets the model defaults - all models inherit from this'
            ),
            'plugins/bart/' => array(
                'config/database.php - sample database file if you want to use the plugin',
                'controllers' => array(
                    'bart_routes_controller.php - api for the the routes part of BART',
                    'bart_stations_controller.php - api for the the stations part of BARTs',
                    'bart_schedules_controller.php - api for the the schedules part of BART',
                ),
                'models' => array(
                    'bart_routes.php - implementation for the the routes part of BART',
                    'bart_stations.php - implementation for the the stations part of BARTs',
                    'bart_schedules.php - implementation for the the schedules part of BART',
                ),
                'views' => array(
                    'bart_routes/*.ctp - views for the the routes part of BART',
                    'bart_stations/*.ctp - views for the the stations part of BARTs',
                    'bart_schedules/*.ctp - views for the the schedules part of BART',
                ),
                'bart_app_controller.php - sets access defaults for the plugin',
                'bart_app_model.php - IMPLEMENTATION OF CACHING, ETC, all plugin models extend this'
            ),
            'views folder' => array(
                'layouts/default.ctp - the default layout for all pages on the site; other dynamic pages are rendered inside this one',
                'pages/readme.ctp - this document',
                'routes/index.ctp - the front page',
            ),
        ),
        'html folder' => array(
            'css - folder that contains site css files',
            'index.php - main gateway for all site requests',
            '/jss/views/* - js files for each of the views in the views folder; views/routes/index is the main one',
            '.htaccess - uses mod_rewrite to redirect all requests to index.php?url=x, where x is the site path requested'
        ),
        '.htaccess - forwards all requests to the html directory, primarily for localhost development'
    )
);

echo $html->nestedList($readme);
?>
<p>
    <a href="http://validator.w3.org/check?uri=referer"><img
            src="http://www.w3.org/Icons/valid-xhtml10"
            alt="Valid XHTML 1.0 Transitional" height="31" width="88" /></a>
</p>
