CREATE DATABASE IF NOT EXISTS `pnore_project2` ;

CREATE TABLE IF NOT EXISTS  `pnore_project2`.`users` (
`id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`username` CHAR( 50 ) NOT NULL ,
`password` CHAR( 50 ) NOT NULL ,
UNIQUE (
`username`
)
) ENGINE = MYISAM ;

ALTER TABLE  `users` CHANGE  `username`  `email` CHAR( 70 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL

CREATE TABLE  `pnore_project2`.`stocks` (
`id` INT( 11 ) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`symbol` CHAR( 11 ) NOT NULL ,
`name` VARCHAR( 255 ) NOT NULL ,
UNIQUE (
`symbol`
)
) ENGINE = MYISAM ;

CREATE TABLE  `pnore_project2`.`users_stocks` (
`id` INT( 11 ) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`user_id` INT( 11 ) UNSIGNED NOT NULL ,
`stock_id` INT( 11 ) UNSIGNED NOT NULL ,
`quantity` INT( 11 ) UNSIGNED NOT NULL
) ENGINE = MYISAM ;

CREATE TABLE  `pnore_project2`.`transactions` (
`id` INT( 11 ) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`user_id` INT( 11 ) UNSIGNED NOT NULL ,
`stock_id` INT( 11 ) UNSIGNED NOT NULL ,
`time` TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL ,
`quantity` INT( 11 ) UNSIGNED NOT NULL COMMENT  'the number of shares of stock_id owned by user with user_id as of this transaction ',
`price` DECIMAL( 8 ) UNSIGNED NOT NULL
) ENGINE = MYISAM ;

--
-- Dumping data for table `users`
--
/* sample - user@user.com and pass */
INSERT INTO `users` (`id`, `username`, `password`) VALUES
(1, 'user@user.com', 'b963733e647f7ac9043fdb4310849be4978d59e4');