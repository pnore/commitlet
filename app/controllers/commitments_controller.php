<?php
class CommitmentsController extends AppController {

	var $name = 'Commitments';
	var $helpers = array('Session');
    var $paginate = array(
        'contain' => 'User'
    );

    function beforeFilter(){
        $this->Auth->allow('*');
        parent::beforeFilter();
	}

	function index() {
		//$this->Commitment->recursive = 0;
        //$fbid = $this->Auth->user('facebook_id');
        $fbid = $this->fb['id'];
        //$res = $this->paginate(array('Commitment.facebook_id' => $fbid ));
        $paginate = array(
            'contain' => 'User',
            'conditions' => array(
                'Commitment.facebook_id' => $fbid
            ),
            'recursive' => 0
        );
        $res = $this->paginate('Commitment');
        debug($res);
        $this->set('commitments', $res);
        /*$this->set('commitments', $this->Commitment->find('list', array(
            'conditions' => array( 'facebook_id' => $this->fb['id'] )
        )));*/
	}

	function stats($id=null) {
		if( $id && $this->Auth->user() 
			&& $this->RequestHandler->isAjax() ) {
				$this->RequestHandler->respondAs('json');
				$this->autoRender=false;
			$stats = $this->Commitment->getStats($id);
            echo json_encode($stats);
		}			
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid commitment', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('commitment', $this->Commitment->read(null, $id));
	}

	function add() {
		if($isAjax = $this->RequestHandler->isAjax()) {
		 //   $this->RequestHandler->respondAs('json');
		 //   $this->autoRender=false;
		}
		if (!empty($this->data) && $this->Auth->user() ) {
			//$this->Commitment->create();
			$this->data['Commitment']['facebook_id'] = 
				$this->fb['id'];
            //Use the following to avoid validation errors:
			//if(! isset( $this->data['Commitment']['start_date'] ) ) {
				$this->data['Commitment']['start_date'] = 
					AppModel::$userNow->format(AppModel::ISO_DATE);
			//}
            unset($this->Commitment->Note->validate['commitment_id']);
            if( $this->Commitment->saveAll($this->data,
                array('validate'=>'first')) ) {
					$this->Commitment->Star->maintainNumFutureStars($this->Commitment->getLastInsertID());
					if($isAjax) {
						echo 'true';
						exit();
					}
                    $this->Session->setFlash(__('The commitment has been saved', true));
                    $this->redirect(array('controller'=>'users','action' => 'index'));
			} else {
				if($isAjax) {
                	echo 'false';
					exit();
				}
				$this->Session->setFlash(__('The commitment could not be saved. Please, try again.', true));
            }   
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid commitment', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Commitment->save($this->data)) {
				$this->Session->setFlash(__('The commitment has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The commitment could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Commitment->read(null, $id);
		}
		$users = $this->Commitment->User->find('list');
		$this->set(compact('users'));
	}

	function delete($id = null) {
		if($isAjax = $this->RequestHandler->isAjax()) {
			   $this->RequestHandler->respondAs('json');
			   $this->autoRender=false;
			if (!$id) {echo 'false';}
			if ($this->Commitment->delete($id)) {echo 'true';}
			exit();
		} else { 
			// we don't really need to support GET deletes any more
			// plus it's a XSFR security liability
			/*
			if (!$id) {
				$this->Session->setFlash(__('Invalid id for commitment', true));
				$this->redirect(array('action'=>'index'));
			}
			if ($this->Commitment->delete($id)) {
				$this->Session->setFlash(__('Commitment deleted', true));
				$this->redirect(array('action'=>'index'));
			}
			$this->Session->setFlash(__('Commitment was not deleted', true));
			$this->redirect(array('action' => 'index'));
			 */
		}
	}
}
?>
