<?php
class StarsController extends AppController {

    var $name = 'Stars';

    function beforeFilter() {
        $this->Auth->allow('*');
        parent::beforeFilter();
    }

    //TODO: add CSRF protection - check to make sure user has permissions to certify and that it was called by ajax from the appropriate page; send inappropriate requests to a black hole
    function certify($commitment_id) {
        $id = $this->Star->certify($commitment_id); 
        if( $this->RequestHandler->isAjax() ) {
            Configure::write('debug', 0); 
            $this->RequestHandler->respondAs('json');
            $this->autoRender = false;
           echo $id;     
        } else {
            $this->set('recentStars', $this->Star->getStars($commitment_id) );
            $this->set('period', $this->Star->period );
        }
    }

    function nullify($commitment_id) {
        $id = $this->Star->nullify($commitment_id); 
        if( $this->RequestHandler->isAjax() ) {
            Configure::write('debug', 0); 
            $this->RequestHandler->respondAs('json');
            $this->autoRender = false;
           echo $id;     
        } else {
            $this->set('recentStars', $this->Star->getStars($commitment_id) );
            $this->set('period', $this->Star->period );
        }

    }


    function index() {
        $this->Star->recursive = 0;
        $this->set('stars', $this->paginate());
    }

    function view($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid star', true));
            $this->redirect(array('action' => 'index'));
        }
        $this->set('star', $this->Star->read(null, $id));
    }

    function add() {
        if (!empty($this->data)) {
            $this->Star->create();
            if ($this->Star->save($this->data)) {
                $this->Session->setFlash(__('The star has been saved', true));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The star could not be saved. Please, try again.', true));
            }
        }
        $commitments = $this->Star->Commitment->find('list');
        $this->set(compact('commitments'));
    }

    function edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Invalid star', true));
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            if ($this->Star->save($this->data)) {
                $this->Session->setFlash(__('The star has been saved', true));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The star could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Star->read(null, $id);
        }
        $commitments = $this->Star->Commitment->find('list');
        $this->set(compact('commitments'));
    }

    function delete($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for star', true));
            $this->redirect(array('action'=>'index'));
        }
        if ($this->Star->delete($id)) {
            $this->Session->setFlash(__('Star deleted', true));
            $this->redirect(array('action'=>'index'));
        }
        $this->Session->setFlash(__('Star was not deleted', true));
        $this->redirect(array('action' => 'index'));
    }
}
?>
