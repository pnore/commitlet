<?php
class UsersController extends AppController {

	var $name = 'Users';

    function beforeFilter(){
        parent::beforeFilter();
        $this->Auth->allow('landing', 'robots');
	}
	
	// Better place for this?
	function robots() {
	    if(Configure::read('debug') >= 1) :
	        $robots_file = "robots_disallow.txt";
	    else:
	        $robots_file = "robots_allow.txt";
	    endif;
	    
	    // Make sure debug is off for the moment so the file is rendered cleanly
	    Configure::write('debug', 0);
	
        $this->view = 'Media';
        $params = array(
              'id' => $robots_file,
              'name' => 'robots',
              'download' => false,
              'extension' => 'txt',
              'path' => APP . 'files' . DS . 'robots' . DS
       );
       $this->set($params);
	}

    function login(){
    }

	function logout(){
	  $this->Auth->logout();
          $this->Session->setFlash('You have logged out of facebook.');
	  $this->redirect('/');
	}

	function landing() {

	}
        
	function index($offset = 0) {
        $this->User->Commitment->recursive = -1;
        $commitments = $this->User->Commitment->find('all', array(
            'conditions'=>array(
                'Commitment.facebook_id'=>$this->fb['id']
            ),
            'fields'=>array(
                //rule and days will be turned into question 
                // by commitments model
                'id','rule','days'
            )));
		//debug($commitments);
        if( $offset > 0 ) { $offset = 0; }
        $this->set('index_offset',$offset);
        $this->User->Commitment->Star->setShift($offset);
        $stars = array();
        foreach($commitments as $key=>$val) {
            $id = $val['Commitment']['id'];
            $this->User->Commitment->Star->recursive = -1;
            $stars[$id] = $this->User->Commitment->Star->getStars($id);
            $period = $this->User->Commitment->Star->period;
        }
        $this->set('commitments', $commitments );
		//debug($stars);
        $this->set('stars', $stars );
		//debug($period);
		if( isset( $period ) ) {
			$this->set('period', $period);
		}
	}

    private function _timezoneable() {
        //$this->User->setUserOffset(-5);
        //$this->User->Commitment->recursive = -1;
        $this->User->recursive = 2;
        $usr = $this->User->find('all', array(
            'conditions'=>array(
                'facebook_id'=>$this->fb['id']
            )
            ));
        $this->set('commitments', $usr );
    }

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for user', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->User->delete($id)) {
			$this->Session->setFlash(__('User deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('User was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}

}
?>
