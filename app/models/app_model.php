<?php 

class AppModel extends Model {

    var $actsAs = array('Containable', 
        'Timezoneable' => array(
            'noOffset'=> array('from','to'),
            'offsetField'=>'timezone'
        ));
 
    static $fbid;
    static $userOffset;
    //var $userOffset;
    static $now; // utc of now on the server
    static $iso; // datetime stamp of $now
    static $isoStr = 'Y-m-d H:i:s';
    static $userNow;
    static $userNowIso;
    const ISO_DATETIME =  'Y-m-d H:i:s'; 
    const ISO_DATE =  'Y-m-d'; 
    var $isoDateString = 'Y-m-d H:i:s';
    
    // see http://cakephp.1045679.n5.nabble.com/Checking-user-session-in-model-td1294422.html
    function setUserState($user) { 
        // Handle authentication data/set local variables in prep for 
        // validation 
        self::$fbid = $user['id'];
        self::$userOffset = $user['timezone'];
        self::$now = new MyDateTime();
        self::$iso = self::$now->format(self::ISO_DATETIME);
        self::$userNow = $this->getUserTime(self::$iso);
        self::$userNowIso = self::$userNow->format(self::ISO_DATETIME);
    }
}
