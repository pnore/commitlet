<?php

/**
 * Assuming that you have most dates stored in the database as UTC,
 * that you are using the Auth component to log users in,
 * and that you are storing the user's time offset in hours 
 * in a field in the users table in the database, this Behavior
 * will convert various field names from UTC to user time and 
 * from user time to UTC time automatically, as specified in 
 * the $actsAs declaration. By default, Timezoneable looks for 
 * a field named "offset" in a "User" model to determine
 * the user offset, though that can be customized by specifying 
 * the userModel and offsetField keys in the $actsAs declaration.
 *
 * There are three types of replacements this behavior can make: 
 * 1.) utcToUser - replace a database utc datetime stamp with 
 * a MyDateTime object representing the user timezone of the datestamp
 * 2.) userToUtc - replace a database user datetime stamp with
 * a MyDateTime object representing the server's time when the user
 * is at the stamp's time 
 * 3.) noOffset - replace a database datetime stamp with a MyDateTime 
 * Object.
 *
 * In order for this behavior to work, you should probably 
 * put the following at the top of your app/config/bootstrap.php:
 * date_default_timezone_set('UTC');
 *
 * @author Peter Nore
 * @date Sunday, December 12, 2010
 **/
class TimezoneableBehavior extends ModelBehavior {

    public $name = 'Timezoneable';    
    var $model;
    //var $userOffset = 0;
    var $utcToUser;
    var $userToUtc;
    var $noOffset;
    var $Model;
    var $primaryKey;
    var $userModel;
    var $sessionKey;
    static $userOffset;

    function setup(&$Model, $settings) {
        if (!isset($this->settings[$Model->alias])) {
            $this->settings[$Model->alias] = array(
                'utcToUser' => array('modified','created'),
                'userToUtc' => array(),
                'noOffset' => array(),
                'userModel' => 'User',
                'offsetField' => 'offset' 
            );
        }
        // remove duplicate values from $settings if 
        // they declared settings in AppModel
        foreach($settings as $key=>$val) {
            if( is_array($val) ) {
                $settings[$key] = array_unique($val);    
            }
        }
        $this->settings[$Model->alias] = array_merge(
            $this->settings[$Model->alias], (array)$settings);
    }

    function afterFind(&$Model, $results) {
        $this->initializeUserOffset($Model);
        if( isset(self::$userOffset) ) {
            $this->offsetAndObjectifyDates(&$Model, $results);
        }
        return $results;
    }   

    function getUserTime(&$Model, $dateTimeString) {
        $datetime = new MyDateTime($dateTimeString);
        if(! isset( self::$userOffset ) ) {
            $this->initializeUserOffset($Model);
        }
        $this->offset(&$datetime,self::$userOffset);
        return $datetime;
    }

    /**
     * Retrieves the datetime of the UTC server when the user 
     * is at the given datetime
     **/
    function getUserDisplacedServerTime(&$Model,$dateTimeString) {
        $datetime = new MyDateTime($dateTimeString);
        if(! isset( self::$userOffset ) ) {
            $this->initializeUserOffset($Model);
        }
        $this->offset(&$datetime,-1*self::$userOffset);
        return $datetime;
    }

    private function initializeUserOffset(&$Model) {
        if(! isset($this->userModel ) ) {
           $this->extractSettings($Model);
        }
        $User = ClassRegistry::init($this->userModel);
        if( isset($User) ) {
            $this->primaryKey = $User->primaryKey;
            $this->sessionKey = 'Auth.'.$this->userModel.'.'.$this->primaryKey; 
            $CakeSession = new CakeSession();
            $primaryKey = $CakeSession->read($this->sessionKey);
            $User->Behaviors->disable('Timezoneable');
			$finder = array( "{$this->primaryKey}" => $primaryKey );
			self::$userOffset = $User->field($this->offsetField, $finder);
            $User->Behaviors->enable('Timezoneable');
        } 
    }

    private function extractSettings(&$Model) {
        //homebrew class extract for convenience
        foreach($this->settings[$Model->alias] as $key=>$value) {
            $this->$key = $value;
        }
    }  

    private function offsetAndObjectifyDates(&$Model, &$arr)
    {
        $this->extractSettings($Model);
        foreach ($arr as $k => &$v)
        {
            if (is_array($v)){
                $this->offsetAndObjectifyDates($Model,$v);
            } else {
                foreach($this->utcToUser as $field){
                    if( $k == $field && (! $v instanceof MyDateTime)){
                        $arr[$k] = $this->getUserTime($Model,$v);
                    }
                } 
                foreach($this->userToUtc as $field){
                    if( $k == $field && (! $v instanceof MyDateTime)){
                        $arr[$k] = 
                            $this->getUserDisplacedServerTime($Model,$v);
                    }
                } 
                foreach($this->noOffset as $field){
                    if( $k == $field && (! $v instanceof MyDateTime) ){
                        $arr[$k] = new MyDateTime($v);
                    }
                } 
            }
        }
    }


    /**
     * Offsets a given MyDateTime by a given number of hours.  
     * param <MyDateTime> $utcMyDateTime - MyDateTime object representing the time to offset - passed by reference
     * param <int> userOffset - the offset from UTC to displace, for example -5 or 7
     * param <string> format - a string to create a date. see http://php.net/manual/en/function.date.php
     **/
    private function offset(&$utcMyDateTime, $userOffset) {
        if(! $utcMyDateTime instanceof MyDateTime ) {
            throw new InvalidArgumentException( 
                "You must supply a MyDateTime object for the first parameter to offset()");
        }
        $interval = new MyDateInterval("PT".abs($userOffset)."H");
        if( $userOffset > 0 ) {
            $utcMyDateTime->add($interval);
        } else if( $userOffset < 0 ) {
            $utcMyDateTime->sub($interval);
        }
    }
}
