<?php

/**
 * Assuming you have all dates stored in the database as UTC,
 * and every model that uses this Behavior has a variable $userOffset
 * representing the current user's offset from UTC in hours 
 * (such as -5 or 2), replaces every specified (datetime) field 
 * with a MyDateTime object offset the number of hours specified.
 *
 * In order for this behavior to work, you must put the following at 
 * the top of your app/config/bootstrap.php:
 * date_default_timezone_set('UTC');
 *
 * @author Peter Nore
 * @date Sunday, December 12, 2010
 **/
class TimezoneableBehavior extends ModelBehavior {

    public $name = 'Utc';    

    function setup(&$Model, $settings = array())
    {
    }

    function afterFind(&$Model)
    {
    }

    function normalize_date(&$Model, $date_str)
    {
        return date('Y-m-d', strtotime(trim($date_str)));
    }

    function test(&$Model) 
    {
        $date = $this->normalize_date($Model, date('Y-m-d'));
        printf('testing behavior %s for model %s on %s', __CLASS__, $Model->name, $date);
    } 

    /**
     * Recursively replaces all fields named "modified" or "created"
     * with MyDateTime objects offset to the user's timezone
     * @author Peter Nore
     **/
    function offsetAndObjectifyDates(&$arr)
    {
        foreach ($arr as $k => &$v)
        {
            if (is_array($v)){
                $this->offsetAndObjectifyDates($v);
            } else {
                if( $k == "modified" || $k == "created" ) {
                    if(! $v instanceof MyDateTime ) {
                        $arr[$k] = $this->getUserTime($v);
                    }
                }
                if( $k == "from" || $k == "to" ) {
                    if(! $v instanceof MyDateTime ) {
                        $arr[$k] = $this->getUserDisplacedServerTime($v);
                    }
                }
            }
        }
    }
   
    function afterFind($results, $primary) {
//        debug(Debugger::trace());
        $this->offsetAndObjectifyDates($results);
        /*
        foreach ($results as $a1 => $a2) {
            foreach( $a2 as $b1 => $b2 ) {
                if( $b1 == "modified" || $b1 == "created") {
                    $results[$a1][$b1] = $this->getUserTime($b2);
                } else {
                    if( (count($b1)>0) && 
                        $b1 == "Commitment" || $b1 == "User" 
                        || $b1 == "Star" || $b1 == "Note" ) {
                        foreach($b1 as $c1 => $c2 ) {
                            if( $c1 == "modified" || $c1 == "created") {
                                $results[$a1][$b1][$c1] = 
                                    $this->getUserTime($c2);
                            } 
                        }
                    }
                }
            }
        }
         */
        //debug($results);
        return $results;
    }   
    
    /**
     * Offsets a given MyDateTime by a given number of hours.  
     * param <MyDateTime> $utcMyDateTime - MyDateTime object representing the time to offset - passed by reference
     * param <int> userOffset - the offset from UTC to displace, for example -5 or 7
     * param <string> format - a string to create a date. see http://php.net/manual/en/function.date.php
     **/
    function offset(&$utcMyDateTime, $userOffset) {
        if(! $utcMyDateTime instanceof MyDateTime ) {
            throw new InvalidArgumentException( 
                "You must supply a MyDateTime object for the first parameter to offset()");
        }
        $interval = new MyDateInterval("PT".abs($userOffset)."H");
        if( $userOffset > 0 ) {
            $utcMyDateTime->add($interval);
        } else if( $userOffset < 0 ) {
            $utcMyDateTime->sub($interval);
        }
    }
    
    function getUserTime($dateTimeString) {
         $datetime = new MyDateTime($dateTimeString);
         $this->offset(&$datetime,self::$userOffset);
         return $datetime;
    }

    /**
     * Retrieves the datetime of the UTC server when the user 
     * is at the given datetime
     **/
    function getUserDisplacedServerTime($dateTimeString) {
         $datetime = new MyDateTime($dateTimeString);
         $this->offset(&$datetime,-1*self::$userOffset);
         return $datetime;
    }
}
