<?php
class Star extends AppModel {

    const NUM_FUTURE_STARS = 3;
    const NUM_PAST_STARS = 5;
    /** Determines how far in the future to create stars **/
    const FUTURE_SEEK = '+1 days';
    const PAST_SEEK = 6;

	var $name = 'Star';
	var $displayField = 'creation';
    /** the period of the latest call to getStars() is stored in this variable **/
    var $period;

	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $belongsTo = array(
		'Commitment' => array(
			'className' => 'Commitment',
			'foreignKey' => 'commitment_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

    /** 
     * returns the stars according to $this->period
     * @param <int> $commitment_id - commitment_id to search for stars for
     */
    function getStars( $commitment_id ) {
        $this->maintainNumFutureStars($commitment_id);
        $period = ($this->period) ? $this->period : $this->setPeriod();

        $from = $period[0]->format(AppModel::ISO_DATE);
        $to = $period[count($period)-1]->format(AppModel::ISO_DATE);

        $finder = array(
            'conditions' => array(
                'commitment_id' => $commitment_id,
            //    'Star.from >=' => $from,
                'Star.to >' => $from,
                'Star.from <=' => $to
            ),
            'order'=>'from ASC'
        );

        // processed by Timezoneable 
        $results = $this->find('all',$finder);
        return $results;
    }

    /**
     * Translates the given MyDatePeriod into an array of iso dates,
     * and sets that array into $this->period
     * @param <MyDateTime or int> $daysBefore - date to search from or days before $toDate
     * @param <MyDateTime> $toDate = defaults to user's now
     */
    function setPeriod($daysBefore = self::PAST_SEEK, $toDate = null) {
        $from = null;
        $to = null;

        if( $toDate instanceof MyDateTime ) {
            $to = $toDate;
        } else {
            $to = clone self::$userNow;
        }

        if( $daysBefore instanceof MyDateTime) {
            $from = $daysBefore;
        } else {
            if( is_int($daysBefore) ) {
                $days = $daysBefore;
            } else {
                $days = self::PAST_SEEK;
            }
            $interval = new MyDateInterval('P'.$days.'D');
            $from = $to->sub($interval);
            $to = clone self::$userNow;
        }

        // both $to and $from should be MyDateTime now
        if(! $to->getTimestamp() > $from->getTimestamp() ){
            // get defaults if something went wrong        
            return $this->setPeriod();
        } 

        $period = array();
        $days =  new MyDatePeriod($from,new MyDateInterval('P1D'),$to);
        foreach($days as $day) {
            array_push($period,$day);
        }                              
        //array_push($period,$to);
        return $this->period = $period;
    }

    function setShift($offset) {
		$offset = $offset * (self::PAST_SEEK + 1);
        $interval = new MyDateInterval('P'.abs($offset).'D');
        $period = $this->setPeriod();
        $from = $period[0];
        $to = $period[count($period)-1];
        if( $offset > 0 ) {
            $this->setPeriod($from->add($interval),$to->add($interval));
        }
        if( $offset < 0 ) {
            $this->setPeriod($from->sub($interval),$to->sub($interval));
        }
    }

    /**
     * Certifies the currently active star.
     **/
    function certify($commitment_id) {
        $stars = $this->getCurrentStar($commitment_id);
        if( $stars ) {
            $this->id = $stars['Star']['id'];
            $this->set('valid',1);
            $this->save();
        }
        return $this->id;
    }
    
    /**
     * Nullifies the currently active star.
     **/
    function nullify($commitment_id) {
        $stars = $this->getCurrentStar($commitment_id);
        if( $stars ) {
            $this->id = $stars['Star']['id'];
            $this->set('valid',0);
            $this->save();
        }
        return $this->id;
    }

    function getCurrentStar($commitment_id) {
        $this->recursive = -1;
        $now = self::$userNowIso;
        $finder = array(
            'conditions' => array(
            'Star.from <=' => $now,
            'Star.to >=' => $now,
            'commitment_id' => $commitment_id ),
            //'fields'=>'Star.id'
        );
        // this is just for the model's benefit
        // no need for extra decoration
        $this->Behaviors->disable('Timezoneable');
        $stars = $this->find('all',$finder);
        $this->Behaviors->enable('Timezoneable');
        if($stars[0]){return $stars[0];}
        return -1;
    }

    /**
     * Fetches an iterator for the given commitment. The iterator
     * goes from the "to" date of the latest star to the current time,
     * in increments of days. If there is no latest star, spans the 
     * entire interval from start_date to present, including the close 
     * of the currently active star. 
     **/
    function getDatetimeIterator($commitment_id) {

        // get the MyDateInterval
        $this->Commitment->recursive = -1;
        $commitment = $this->Commitment->findById($commitment_id);
        $days = $commitment['Commitment']['days'];
        $interval = new MyDateInterval("P".$days."D");

        // get startDatetime
        $numStars = $this->getNumStars($commitment_id);
        if( $numStars < 1 ) { // we don't have any stars
            $startDatetime = 
                new MyDateTime($commitment['Commitment']['start_date']);
        } else { // find latest star
            $finder = array(
                'conditions' => array(
                    'commitment_id' => $commitment_id ),
                'order'=>'Star.to DESC'
            );
            $last = $this->find('first',$finder);
            $startDatetime = $last['Star']['to'];
        }

        // get endDatetime
        $endDatetime = new MyDateTime(self::FUTURE_SEEK);

        $dates = new MyDatePeriod($startDatetime, $interval, $endDatetime->add($interval));
        return $dates;
    }

    function getNumStars($commitment_id) {
        $finder = array(
            'conditions' => array(
                'commitment_id' => $commitment_id )
            );
        return $this->find('count',$finder);
    }
    
    function createStars($commitment_id, $datetimeIterator) {
        $prev = null;
        foreach($datetimeIterator as $curr) {
            if( isset($prev) ) {
                $this->create();
                $this->set('commitment_id',$commitment_id);
                $this->set('from',
                    $prev->format(self::ISO_DATE));
                $this->set('to',$curr->format(
                    self::ISO_DATE));
                $this->save();
            }
            $prev = $curr;
        }
    }

    /**
     * updates the number of future stars to 
     * self::NUM_FUTURE_STARS if necessary.
     **/
    function maintainNumFutureStars($commitment_id) {
        $this->recursive = -1;
        $now = self::$userNowIso;
        $finder = array(
            'conditions' => array(
                'to >=' => $now,
                'commitment_id' => $commitment_id )
        );
        // we need to extract the ACTUAL time from the db, even though
        // it's one of the translated times
        $this->Behaviors->disable('Timezoneable');
        $numFutureStars = $this->find('count',$finder);
        $this->Behaviors->enable('Timezoneable');
        if($numFutureStars < 1) {
            $period = $this->getDatetimeIterator($commitment_id);
            $this->createStars($commitment_id, $period);
        }
        //return $period;
    } 

	function getFromDateOfLatestStar($id) {
		$finder = array(
			'conditions' => array(
				'commitment_id' => $id,
				'Star.from <=' => AppModel::$userNowIso
			),
			'fields'=>array('from'),
			'order'=>'from DESC',
			'limit'=>1
		);
		// processed by Timezoneable 
		$this->recursive=-1;
		//$this->Behaviors->disable('Timezoneable');
		$res = $this->find('all',$finder);
		//$this->Behaviors->enable('Timezoneable');
		if( count($res) > 0 ){
			$res = $res[0];
			return $res['Star']['from'];
		}
        return null;
	}

}
?>
