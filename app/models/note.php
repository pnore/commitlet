<?php
class Note extends AppModel {
	var $name = 'Note';
	var $displayField = 'text';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $belongsTo = array(
		'Commitment' => array(
			'className' => 'Commitment',
			'foreignKey' => 'commitment_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
        ),
	);
}
?>
