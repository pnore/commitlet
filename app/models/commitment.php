<?php
class Commitment extends AppModel {

	var $name = 'Commitment';

	var $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'facebook_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
    );

	var $hasMany = array(
		'Star' => array(
			'className' => 'Star',
			'foreignKey' => 'commitment_id',
			'dependent' => true,
			'conditions' => '',
			'fields' => '',
			'order' => 'Star.from DESC',
			'limit' => '10',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
        'Note' => array(
			'className' => 'Note',
			'foreignKey' => 'commitment_id',
			'dependent' => true,
			'conditions' => '',
			'fields' => '',
			'order' => 'Note.created DESC',
			'limit' => '10',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

	static $i = 0;

    function afterFind($results, $primary) {
        foreach ($results as $key => $val) {
            $rule = (isset($val['Commitment']['rule']))?$val['Commitment']['rule']:null;
            $id = (isset($val['Commitment']['id']))?$val['Commitment']['id']:null;
            //$days = (isset($val['Commitment']['days']))?$val['Commitment']['days']:null;
			if( $rule && $id ) {
				$date = $this->Star->getFromDateOfLatestStar($id);
				if( $date instanceof MyDateTime ) {
					$phrase = $this->getCommitmentPhrase( $rule, $date );
					$results[$key]['Commitment']['question'] = $phrase;
				}
			}
        }
        return $results;
        //return parent::afterFind($results,$primary);
    }


	function getStats($id) {
		$finder = array(
			'conditions' => array(
				'Star.from <=' => AppModel::$userNowIso,
				'Star.commitment_id' => $id
				)
		);	
		$total = $this->Star->find('count', $finder );
		$finder['conditions']['valid'] = 1;
		$valid = $this->Star->find('count', $finder );
		$average = (string)($total < 1) ? 0 : $this->percent($valid,$total);
		$date = $this->read('start_date', $id);
		$date = new MyDateTime( $date['Commitment']['start_date'] );
		$date = $date->format('D M j, Y');
		$ret = array();
		$ret['Commitment']['valid'] = $valid;
		$ret['Commitment']['total'] = $total;
		$ret['Commitment']['average'] = $average;
		$ret['Commitment']['date'] = $date;
		$ret['User']['userNow'] = self::$userNowIso;
		$ret['User']['now'] = self::$iso;
		$another = $this->getUserTime(self::$iso);
		$ret['User']['recalculatedUserNow'] = $another;
		$ret['User']['another_formulation'] = self::$userNow->format(self::ISO_DATETIME);
		$ret['User']['recalc2'] = $another->format(self::ISO_DATETIME);
			/*
		$ret = array();
		$ret['Commitment']['valid'] = 43;
		$ret['Commitment']['total'] = 48;
		$ret['Commitment']['average'] = 88.3;
		$ret['Commitment']['grade'] = 'B';
		$ret['Commitment']['date'] = '2010-5-15';
			 */
		return $ret;
	}

	function percent($num_amount, $num_total) {
		$count = 100*($num_amount / $num_total);
		return number_format($count, 1);
	}

    /**
     * Returns the commitment statment for the given 
     * commitment rule and the number of days. 
     * @author Peter Nore
     * @parameters you must set Commitment.rule and Commitment.days 
     * before using this function
     * @return <string> the Commitment phrase
     */
    function getCommitmentPhrase( $rule, $date ) {
		$days = $date->diff( AppModel::$userNow );
        if( $rule!=null  && $days!=null ) {
			// days is "days ago" 
            $days = $days->days;
            if( is_numeric($days) && is_int($days) ) {
                switch( $days ) {
                    case 0:
                        return "Have I $rule today?";
                        break;
                    case 1:
                        return "Have I $rule yesterday or today?";
                        break;
                }
				if( $days > 1 ) {
					$prefix = "Have I $rule since ";
					$dateFormat = '';
					$suffix = "?";
					if( $days < 7 ) {
						// "Sunday"
						$dateFormat = 'l';
					} else if( $days >= 7 && $days < 300 ) {
						// Sunday, February 23
						$dateFormat = 'l, F n';
					} else if( $days >= 300 ) {
						// Sunday, February 23, 2009
                    	$dateFormat = 'l, F n, Y';
					}	
					return $prefix . $date->format($dateFormat) . $suffix; 
				}
            }    
        } return '';
    }

}




// unused??
    /**
     * Returns a plain text description of the date $numDaysAgo.
     * @author Peter Nore
     * @param <int> number of days ago, zero for today
     * @return <string> text date: Saturday, December 4th
     **/
	/*
    static function getDaysAgoEnglish($date, $days) {
       //$ago = strtotime("-$numDaysAgo days");
	   $now = clone AppModel::$userNow;
	   $ago = $;
       if( $numDaysAgo < 7 ) {
            return strftime('%A', $ago);
       }
       if( $numDaysAgo > 360 ) {
            return date('F jS, Y', $ago);
       }
       //return strftime('%A, %B%e',$ago);
       return date('l, F jS', $ago);
    }
	 */

    /**
     * Checks the modified date of the commitment, which is
     * updated every time stars are checked. If the 
     * interval between right now and the modified date
     * is greater than the period of the commitment,
     * fills in the stars between the last created star
     * and today with invalid stars.
     **
    function checkForAnomalies($id = null) {
        if( $id == null ) { $id = $this->id; }
            $this->recursive = -1;
        $commitment = $this->findById($id);
        if(! count($commitment > 0) ) { return false; } 
        //debug($commitment);
        $lastCache = $commitment['Commitment']['modified'];
        $created = $commitment['Commitment']['created'];
        if ($lastCache->getTimestamp() == $created->getTimestamp() ){
            // needs initialization
        } else {
            $today = new MyDateTime();
            $diff = $today->diff($lastCache);
            if($diff->d > $commitment['Commitment']['days']){
                $dateIterator = 
                $this->getPossibleDatesSinceModified($commitment, $today);
            }
        }
        if($needsInitialization || $needsStarsFilledIn  ) {
            //debug("okay, we're going to fill it in" );
            $this->createInvalidStars($id,$dateIterator);
            $this->id = $id;
            $this->saveField('modified',$today->format($isoDateString));
        }
    }

    function getPossibleDatesSinceModified($commitment, $target) {
        $id = $commitment['Commitment']['id'];
        $lastModified = $commitment['Commitment']['modified'];
        $origin = new MyDateTime($commitment['Commitment']['start_date']);
        $days = $commitment['Commitment']['days'];
        $interval = new MyDateInterval("P".$days."D");
        $period = new MyDatePeriod($origin, $interval, $target);
        // see at the bottom of this file for DateFilterIterator
        $df = new DateFilterIterator($period, $lastModified );
        return $df;
	}  */
?>
