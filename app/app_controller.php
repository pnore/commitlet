<?php

App::Import('ConnectionManager');

class AppController extends Controller {

    /** Make the SessionComponent and AuthComponent available to all Controllers * */
    var $components = array('DebugKit.Toolbar', 'Session', 'Auth', 'Facebook.Connect', 'RequestHandler');

    /** Make the HtmlHelper, FormHelper, and SessionHelper available to all views * */
    var $helpers = array('Html', 'Form', 'Session', 'Facebook.Facebook', 'Star', 'Js'=>'Jquery');
    var $fb;
    var $userMyDateTimeString = 'F jS, Y g:i a';
    var $userTimeString = 'g:i a';
    var $userDateString = 'F jS';
    var $paramsForJs = array();
    const USER_SHORT_DATE = 'D M j';

    function beforeFilter() {
        // see http://www.webtechnick.com/wtn/blogs/view/238/Facebook_Plugin_v2_0_Graph_System
        //Get all the details on the facebook user
        $this->fb = $this->Connect->user(); 
        $this->set('fb',$this->fb );
        $this->set('userMyDateTimeString', $this->userMyDateTimeString);
        $this->set('userTimeString', $this->userTimeString);
        $this->set('userDateString', $this->userDateString);
        $this->set('userShortDate', self::USER_SHORT_DATE);
        if ($this->RequestHandler->isAjax()) {
			$this->autoLayout = false;
            Configure::write('debug', 0); 
            //$this->RequestHandler->respondAs('json');
            //$this->autoRender = false;
        } else {
//            $this->paramsForJs['users_index'] = 'http://commitlet.com/users/index/';
            $this->paramsForJs['users_index'] = 'http://'.$_SERVER['SERVER_NAME'].'/users/index/';
            $this->paramsForJs['stars_certify'] = $this->getUrl(array('controller' => 'stars', 'action' => 'certify'));
            $this->paramsForJs['stars_nullify'] = $this->getUrl(array('controller' => 'stars', 'action' => 'nullify'));
            $this->paramsForJs['fb'] = $this->fb;
            $this->set('paramsForJs', $this->paramsForJs);
        }
        // see http://cakephp.1045679.n5.nabble.com/Checking-user-session-in-model-td1294422.html
        $this->{$this->modelClass}->setUserState($this->fb); 
		$this->Auth->loginAction = array('controller'=>'users', 'action'=>'landing');
		$this->Auth->loginRedirect = array( 'controller'=>'users', 'action'=>'index' );
		$this->Auth->logoutRedirect = array( 'controller'=>'users',
			'action'=>'landing' );
		$this->set('loginMsg', (($this->Auth->user())? "logged in" : "logged out" ));
    }

    function getUrl($arr) {
        $urlWithWebrootInMiddle = Router::url($arr, false);
        $nameOfWebrootDir = "/" . WEBROOT_DIR;
        $niceUrl = "http://";
        $niceUrl .= $_SERVER['HTTP_HOST'];
        $niceUrl .= str_replace($nameOfWebrootDir, "", $urlWithWebrootInMiddle);
        return $niceUrl;
    }

    /**
     * Called by app/plugins/facebook/controllers/components/connect.php
     * updates the modified field of the user to last login and 
     * updates timezone upon login
     **/
    function __afterLogin($loginData) {
		if( $this->Auth->user() ) {
			$User = ClassRegistry::init('User');
			if( $loginData['id'] != 0 ) {
				$fbid = $loginData['id'];
				// the time object will confuse set
				$User->Behaviors->disable('Timezoneable');
			    $ret = $User->read(null,$fbid);
				$User->Behaviors->enable('Timezoneable');
				$User->set('timezone',$loginData['timezone']);
				$User->save();
				//$this->Session->setFlash( $fbid );
				//$User->facebook_id = $fbid;
				//$this->Session->setFlash( ($User->saveField('timezone', $fbid)) ? 'it saved' : 'it did not save' );
			}
		}
		$this->redirect(array(
			'controller'=>'users',
			'action'=>'index'
		));
    }

    function __afterLogout() {
		$this->Auth->logout();
		$this->redirect(array(
			'controller'=>'users',
			'action'=>'landing'
		));
    }
}

?>
